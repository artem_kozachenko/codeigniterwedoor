<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
                <!-- general form elements -->
                <div id="message"></div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add new agent</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <?php echo form_open("admin/add_agent_request", array("id" => "new_agent_form", "enctype" => "multipart/form-data")) ?>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="first_name">First name</label>
                                <input name="first_name" id="first_name" type="text" class="form-control" placeholder="Enter first name">
                            </div>
                            <div class="form-group">
                                <label for="second_name">Second name</label>
                                <input name="second_name" id="second_name" type="text" class="form-control" placeholder="Enter second name">
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input data-toggle="tooltip" name="email" id="email" type="text" class="form-control" placeholder="Enter email" title="Email can't be longer than 35 characters. Can contain only Latin characters, numbers and symbols (. - _)" data-placement="top">
                            </div>
                            <div class="form-group">
                                <label for="phone_number">Phone number</label>
                                <input name="phone_number" id="phone_number" type="text" class="form-control" placeholder="Enter phone number">
                            </div>
                            <div class="form-group">
                                <label for="skype">Skype</label>
                                <input name="skype" id="skype" type="text" class="form-control" placeholder="Enter skype">
                            </div>
                            <div class="form-group">
                              <label for="about">About</label>
                              <textarea name="about" id="about" class="form-control a" rows="3"></textarea>
                            </div>
                            <div class="form-group" id="a">
                                <label for="photo">Add photo</label>
                                <input name="photo" type="file" id="photo" class="a">
                            </div>
                            <p class="help-block">Max photo size - 2 mb</p>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button name="new_agent_button" type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready (function ()
    {
        $('[data-toggle="tooltip"]').tooltip();
        var files = '';
        $('#photo').on('change', function()
        {
            files = this.files;
        });
        $('#new_agent_form').submit(function(e)
        {
            e.preventDefault();
            var data = new FormData();
            var me = $(this);
            $.each($('input'), function(index, form_elements)
            {
                id = form_elements.id;
                if (id != 'photo')
                {
                    data.append(id , $('#' + id).val());
                }
            });
            data.append('about', $('#about').val());
            if(files != '')
            {
                $.each(files, function(index, value)
                {
                    data.append('photo', value);
                });
            }
            for (var pair of data.entries()) {
                console.log(pair[0]+ ', ' + pair[1]);
            }
            $.ajax(
            {
                url: me.attr('action'),
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                dataType: 'json',
                success: function(response)
                {
                    if(response.success == true)
                    {
                        $('.form-group')
                        .removeClass('has-error')
                        .addClass('has-success');
                        $('.text-danger')
                        .remove();
                        if(response.photo_success == true)
                        {
                            $('#message').append(
                                '<div class="alert alert-success">'+
                                'New agent was successfully added!'+
                                '</div>'
                            );
                            $('.alert-success').delay(500).show(10, function()
                            {
                                $(this).delay(3000).hide(10, function()
                                {
                                    $(this).remove();
                                });
                            });
                            me[0].reset();
                            $('.form-group')
                            .removeClass('has-success');
                            $('.text-danger').remove();
                        }
                        else
                        {
                            var error = response.messages.photo;
                            var element = $('#photo');
                            element.closest('div.form-group')
                            .removeClass('has-error')
                            .addClass(error.length > 0 ? 'has-error' : 'has-success')
                            .find('.text-danger')
                            .remove();
                            element.after(error);
                        }
                    }
                    else
                    {
                        $.each(response.messages, function(key, value)
                        {
                            var element = $('#' + key);
                            element.closest('div.form-group')
                            .removeClass('has-error')
                            .addClass(value.length > 0 ? 'has-error' : 'has-success')
                            .find('.text-danger')
                            .remove();
                            element.after(value);
                        });
                    }
                }
            });
        });
    });
</script>
