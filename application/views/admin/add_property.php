<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
                <!-- general form elements -->
                <div id="message"></div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add new property</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <?php echo form_open("admin/add_property_request", array("id" => "new_property_form", "enctype" => "multipart/form-data")) ?>
                        <div class="box-body">


                            <div class="form-group">
                                <label for="name">Name</label>
                                <input name="name" id="name" type="text" class="form-control" placeholder="Enter name">
                            </div>


                            <div class="form-group">
                                <label for="price">Price</label>
                                <input name="price" id="price" type="text" class="form-control" placeholder="Enter price in $">
                            </div>


                            <div class="form-group">
                                <label for="city">City</label>
                                <input name="city" id="city" type="text" class="form-control" placeholder="Enter city">
                            </div>


                            <div class="form-group">
                                <label for="area">Area</label>
                                <input name="area" id="area" type="text" class="form-control" placeholder="Enter area">
                            </div>


                            <div class="form-group">
                                <label for="garage">Garages</label>
                                <input name="garage" id="garage" type="text" class="form-control" placeholder="Enter number of garages">
                            </div>


                            <div class="form-group">
                                <label for="bedroom">Bedrooms</label>
                                <input name="bedroom" id="bedroom" type="text" class="form-control" placeholder="Enter number of bedrooms">
                            </div>


                            <div class="form-group">
                                <label for="bathroom">Bathrooms</label>
                                <input name="bathroom" id="bathroom" type="text" class="form-control" placeholder="Enter number of bathrooms">
                            </div>


                            <div class="form-group">
                                <label for="acres">Acres</label>
                                <input name="acres" id="acres" type="text" class="form-control" placeholder="Enter acres">
                            </div>


                            <div class="form-group">
                                <label for="heat">Heat</label>
                                <input name="heat" id="heat" type="text" class="form-control" placeholder="Enter heat">
                            </div>


                            <div class="form-group">
                                <label for="dimensions">Dimensions</label>
                                <input data-toggle="tooltip" name="dimensions" id="dimensions" type="text" class="form-control" placeholder="Enter dimensions"  title="Format: .x." data-placement="top">
                            </div>


                            <div class="form-group">
                                <label for="size_source">Size source</label>
                                <input name="size_source" id="size_source" type="text" class="form-control" placeholder="Enter size source">
                            </div>


                            <div class="form-group">
                                <label for="ac">AC</label>
                                <input name="ac" id="ac" type="text" class="form-control" placeholder="Enter AC">
                            </div>


                            <div class="form-group">
                              <label for="description">Description</label>
                              <textarea name="description" id="description" class="form-control" rows="3"></textarea>
                            </div>


                            <!-- radio -->
                            <div class="form-group">
                                <label>
                                    <input id="type_1" name="type" type="radio" name="type" value="Sale"> Sale
                                </label>
                                <br>
                                <label>
                                    <input id="type_2" name="type" type="radio" name="type" value="Rent"> Rent
                                </label>
                                <div id="type_error"></div>
                            </div>


                            <div class="form-group">
                                <label>Photos</label>
                                <!-- <input type="file" name="photo_1" id="photo_1" multiple accept="image/jpg,image/png"> -->
                                <input type="file" name="photos[]" id="photos" multiple accept="image/png,image/jpg">
                                <!-- <div id='photos'></div> -->
                            </div>
                            <p class="help-block">
                                You can choose up to five photos!
                                <br>
                                Max photo size - 4 mb
                            </p>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button name="new_property_button" type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready (function ()
    {
        $('[data-toggle="tooltip"]').tooltip();
        var files = '';
        $('#photos').on('change', function()
        {
            files = this.files;
        });
        $('#new_property_form').submit(function(e)
        {
            e.preventDefault();
            var data = new FormData();
            var me = $(this);
            $.each($('input'), function(index, form_elements)
            {
                id = form_elements.id;
                if (id != 'photos' && id != 'type_1' && id != 'type_2')
                {
                    data.append(id , $('#' + id).val());
                }
            });
            data.append('description', $('#description').val());
            if(document.getElementById('type_1').checked || document.getElementById('type_2').checked)
            {
                data.append('type', $('input[type="radio"]:checked').val());
            }
            else
            {
                data.append('type', '');
            }
            if(files != '')
            {
                $.each(files, function(index, value)
                {
                    data.append('photos[]', value);
                });
            }
            $.ajax(
            {
                url: me.attr('action'),
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                dataType: 'json',
                success: function(response)
                {
                    if(response.success == true)
                    {
                        $('.form-group')
                        .removeClass('has-error')
                        .addClass('has-success');
                        $('.text-danger')
                        .remove();
                        if(response.photo_success == true)
                        {
                            $('#message').append(
                                '<div class="alert alert-success">'+
                                'New property was successfully added!'+
                                '</div>'
                            );
                            $('.alert-success').delay(500).show(10, function()
                            {
                                $(this).delay(3000).hide(10, function()
                                {
                                    $(this).remove();
                                });
                            });
                            me[0].reset();
                            $('.form-group')
                            .removeClass('has-success');
                            $('.text-danger').remove();
                        }
                        else
                        {
                            var error = response.messages.photo;
                            var element = $('#photo');
                            element.closest('div.form-group')
                            .removeClass('has-error')
                            .addClass(error.length > 0 ? 'has-error' : 'has-success')
                            .find('.text-danger')
                            .remove();
                            element.after(error);
                        }
                    }
                    else
                    {
                        $.each(response.messages, function(key, value)
                        {
                            if (key != 'type')
                            {
                                var element = $('#' + key);
                                element.closest('div.form-group')
                                .removeClass('has-error')
                                .addClass(value.length > 0 ? 'has-error' : 'has-success')
                                .find('.text-danger')
                                .remove();
                                element.after(value);
                            }
                            else
                            {
                                var element = $('#type_1');
                                element.closest('div.form-group')
                                .removeClass('has-error')
                                .addClass(value.length > 0 ? 'has-error' : 'has-success')
                                .find('.text-danger')
                                .remove();
                                $('#type_error').after(value);
                            }
                        });
                    }
                }
            });
        });
    });
</script>
