<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Log in</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?php echo base_url() ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url() ?>resources/dist/css/AdminLTE.min.css">
        <!-- jQuery 3 -->
        <script src="<?php echo base_url() ?>bower_components/jquery/dist/jquery.min.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <b>Admin</b>LTE</a>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg">Sign in to start your session</p>
                <?php echo form_open("admin/log_in_request", array("id" => "log_in_form")) ?>
                    <div class="form-group has-feedback">
                        <input id="email" name="email" type="text" class="form-control" placeholder="Email">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input id="password" name="password" type="password" class="form-control" placeholder="Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-xs-12">
                            <button name="log_in_button" type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                        </div>
                        <div id="succes">

                        </div>
                        <!-- /.col -->
                    </div>
                </form>
            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->
        <script>
            $('#log_in_form').submit(function(e)
            {
                e.preventDefault();
				var me = $(this);
                $.ajax(
                {
                    url: me.attr('action'),
                    type: 'POST',
                    data: me.serialize(),
                    dataType: 'json',
                    success: function(response)
                    {
                        if(response.success == true)
                        {
                            window.location.href = "http://wedoor.com/admin/add_agent";
                        }
                        else
                        {
                            $.each(response.messages, function(key, value)
                            {
                                var element = $('#' + key);
                                element.closest('div.form-group')
                                .removeClass('has-error')
                                .addClass(value.length > 0 ? 'has-error' : 'has-success')
                                .find('.text-danger')
                                .remove();
                                element.after(value);
                            });
                        }
                    }
                });
            });
        </script>
    </body>
</html>
