<?php
    $query = $this->get_data->one_agent($agent_id);
    $_SESSION['agent_id'] = $agent_id;
    foreach($query->result_array() as $row)
    {
        $first_name = $row['first_name'];
        $second_name = $row['second_name'];
        $photo = $row['photo'];
        $phone_number = $row['phone_number'];
        $email = $row['email'];
        $skype = $row['skype'];
        $about = $row['about'];
    }
?>
    <!-- Header bradcrumb -->
    <header class="bread_crumb">
        <div class="pag_titl_sec">
            <h1 class="pag_titl"> scott berends </h1>
            <h4 class="sub_titl"> Echo Park occupy mustache gastropub </h4>
        </div>
    </header>





    <div class="spacer-60"></div>





    <div class="container">
        <div class="row">
            <div id="a"></div>
            <!-- Agent Section -->
            <section id="agent_sec" class="col-md-12">
                <!-- Agent Info -->
                <div class="agen_info2">
                    <div class="col-md-5">
                        <?php
                            echo '<img class="img-responsive img-hover" style="width: 100%; height: 270px;" src="'.$photo.'">';
                        ?>
                        <div class="cont_frm2">
                            <h2 class="frm_titl"> CONTACT ME </h2>
                            <?php echo form_open("agents/message_request", array("id" => "contact_agent")) ?>
                                <div class="control-group form-group first">
                                    <div class="form-group">
                                        <input name="name" type="text" class="form-control" id="name" placeholder="Your Name">
                                        <p class="help-block"></p>
                                    </div>
                                    <div class="form-group">
                                        <input data-toggle="tooltip" name="email" type="text" class="form-control" id="email" placeholder="Email Address" title="Email can't be longer than 35 characters. Can contain only Latin characters, numbers and symbols (. - _)" data-placement="top">
                                        <p class="help-block"></p>
                                    </div>
                                    <div class="form-group">
                                        <textarea name="message" class="form-control" id="message" placeholder="Message Box"></textarea>
                                    </div>
                                    <button name="message_button" type="submit" class="btn btn-primary">Send Message</button>
                                    <div id="success"></div>
                                    <!-- For success/fail messages -->
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row agen_desc">
                                    <div class="col-md-7">
                                        <h3 class="sec_titl">
                                            <?php echo $first_name.' '.$second_name; ?>
                                         </h3>
                                        <p class="sec_desc">
                                            Buying Agents
                                        </p>
                                    </div>
                                    <div class="col-md-5 p0">
                                        <div class="soc_icon">
                                            <a href="#">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <p class="sec_desc">
                                    <?php echo $about; ?>
                                </p>
                                <div class="panel_bottom">
                                    <div class="agen_feat">
                                        <p class="area">
                                            <a href="tel:910-213-7890">
                                                <i class="fa fa-phone"></i>
                                                <?php echo $phone_number; ?>
                                            </a>
                                        </p>
                                        <p class="bedrom">
                                            <a href="mailto:scott@berends.com?Subject=Agent%20enquiry">
                                                <i class="fa fa-envelope"></i>
                                                <?php echo $email; ?>
                                            </a>
                                        </p>
                                        <p class="bedrom">
                                            <a href="skype:-scottberends1-?chat">
                                                <i class="fa fa-skype"></i>
                                                <?php echo $skype; ?>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- /.row -->
            </section>
            <div class="spacer-60"></div>
        </div>
    </div>

<script>
$(document).ready (function ()
{
    $('[data-toggle="tooltip"]').tooltip();
    $('#contact_agent').submit(function(e)
    {
        e.preventDefault();
        var me = $(this);
        $.ajax(
        {
            url: me.attr('action'),
            data: me.serialize(),
            type: 'POST',
            dataType: 'json',
            success: function(response)
            {
                if(response.success == true)
                {
                    $('.form-group')
                    .removeClass('has-error')
                    .addClass('has-success');
                    $('.text-danger')
                    .remove();
                    if (response.send == true) {
                        $('#a').append(
                            '<div class="alert alert-success text-center">'+
                            'Message was successfully sent!'+
                            '</div>'
                        );
                        $('.alert-success').delay(500).show(10, function()
                        {
                            $(this).delay(3000).hide(10, function()
                            {
                                $(this).remove();
                            });
                        });
                        me[0].reset();
                        $('.form-group')
                        .removeClass('has-success');
                        $('.text-danger').remove();
                    }
                    else
                    {
                        $('#a').append(
                            '<div class="alert alert-danger text-center">'+
                            'Message wasn\'t sent!'+
                            '</div>'
                        );
                        $('.alert-success').delay(500).show(10, function()
                        {
                            $(this).delay(3000).hide(10, function()
                            {
                                $(this).remove();
                            });
                        });
                        me[0].reset();
                        $('.form-group')
                        .removeClass('has-success');
                        $('.text-danger').remove();
                    }
                }
                else
                {
                    $.each(response.messages, function(key, value)
                    {
                        var element = $('#' + key);
                        element.closest('div.form-group')
                        .removeClass('has-error')
                        .addClass(value.length > 0 ? 'has-error' : 'has-success')
                        .find('.text-danger')
                        .remove();
                        element.after(value);
                    });
                }
            }
        });
    });
});

</script>
