        <!-- Header bradcrumb -->
        <header class="bread_crumb">
            <div class="pag_titl_sec">
                <h1 class="pag_titl"> our agents </h1>
                <h4 class="sub_titl"> Echo Park occupy mustache gastropub </h4>
            </div>
        </header>
        <div class="spacer-60"></div>





        <div class="container">
            <div class="row">
                <!-- Agent Section -->
                <section id="agent_sec" class="col-md-12">
                    <?php
                        // Устанавливаем количество записей, которые будут выводиться на одной странице
                        // Поставьте нужное вам число. Для примера я указал одну запись на страницу
                        $agent_limit = 5;
                        // Ограничиваем количество ссылок, которые будут выводиться перед и
                        // после текущей страницы$page_number
                        $limit = 1;
                        // Если значение page= не является числом, то показываем
                        // пользователю первую страницу
                        if(!is_numeric($page_number)) $page_number=1;
                        // Узнаем количество всех доступных записей
                        $agent_count = $this->db->count_all_results('agents');
                        // Вычисляем количество страниц, чтобы знать сколько ссылок выводить
                        $recuired_pages = $agent_count / $agent_limit;
                        // Округляем полученное число страниц в большую сторону
                        $recuired_pages = ceil($recuired_pages);
                        // Если пользователь вручную поменяет в адресной строке значение page= на нуль или больше $required_pages,
                        // то мы определим это и поменяем на единицу, то-есть отправим на первую
                        // страницу, чтобы избежать ошибки
                        if ($page_number < 1 OR $page_number > $recuired_pages)
                        {
                            redirect('agents/all/1');
                        }
                        // Здесь мы увеличиваем число страниц на единицу чтобы начальное значение было
                        // равно единице, а не нулю. Значение page_number= будет
                        // совпадать с цифрой в ссылке, которую будут видеть посетители
                        $recuired_pages++;
                        // Если значение page_number= больше числа страниц, то выводим первую страницу
                        if ($page_number > $recuired_pages) $page_number = 1;
                        // Переменная $list указывает с какой записи начинать выводить данные.
                        // Если это число не определено, то будем выводить
                        // с самого начала, то-есть с нулевой записи
                        if (!isset($list)) $list=0;
                        // Чтобы у нас значение page_number= в адресе ссылки совпадало с номером
                        // страницы мы будем его увеличивать на единицу при выводе ссылок, а
                        // здесь наоборот уменьшаем чтобы ничего не нарушить.
                        $list = --$page_number * $agent_limit;
                        // Делаем запрос подставляя значения переменных $agent_limit и $list
                        $query = $this->get_data->all_agents($agent_limit, $list);
                        foreach ($query->result_array() as $row)
                        {
                            $about = $row['about'];
                            $about = substr($about, 0, 200);
                            $about .= '...';
                            echo "<div class='row'>";
                            echo "<div class='agen_info'>";
                            echo "<div class='col-md-4'>";
                            echo "<a href='/agents/id/".$row['agent_id']."'>";//СДЕЛАТЬ НОРМАЛЬНУЮ ССЫЛКУ НА ПРОФИЛЬ
                            echo "<img class='img-responsive img-hover agent_photo' src='".$row['photo']."'>";
                            echo "</a>";
                            echo "</div>";
                            echo "<div class='col-md-8'>";
                            echo "<div class='panel panel-default agent_div'>";
                            echo "<div class='panel-body'>";
                            echo "<div class='row agen_desc'>";
                            echo "<div class='col-md-8'>";
                            echo "<h3 class='sec_titl'>";
                            echo "<a href='/agents/id/".$row['agent_id']."'>".$row['first_name']." ".$row['second_name']."</a>";//СДЕЛАТЬ НОРМАЛЬНУЮ ССЫЛКУ НА ПРОФИЛЬ
                            echo "</h3>";
                            echo "<p class='sec_desc'>";
                            echo "Buying Agents";
                            echo "</p>";
                            echo "</div>";
                            echo "</div>";
                            echo "<div class='about_height'>";
                            echo "<p class='sec_desc'>";
                            echo $about;
                            echo "</p>";
                            echo "</div>";
                            echo "<div class='panel_bottom '>";
                            echo "<div class='agen_feat panel_bottom_info'>";
                            echo "<p class='area'>";
                            echo "<a href='tel:910-213-7890'>";
                            echo "<i class='fa fa-phone'></i>";
                            echo $row['phone_number'];
                            echo "</a>";
                            echo "</p>";
                            echo "<p class='bedrom'>";
                            echo "<a href='#'>";
                            echo "<i class='fa fa-envelope'></i>";
                            echo $row['email'];
                            echo "</a>";
                            echo "</p>";
                            echo "<p class='bedrom'>";
                            echo "<a href='#'>";
                            echo "<i class='fa fa-skype'></i>";
                            echo $row['skype'];
                            echo "</a>";
                            echo "</p>";
                            echo "</div>";
                            echo "</div>";
                            echo "</div>";
                            echo "</div>";
                            echo "</div>";
                            echo "</div>";
                            echo "</div>";
                            echo "<div class='spacer-30'></div>";
                        }
                        echo 'Страницы: ';
                        // _________________ начало блока 1 _________________
                        // Выводим ссылки "назад" и "на первую страницу"
                        if ($page_number>=1)
                        {
                            // Значение page_number= для первой страницы всегда равно единице,
                            // поэтому так и пишем
                            echo '<a href="1"><<</a> &nbsp; ';
                            // Так как мы количество страниц до этого уменьшили на единицу,
                            // то для того, чтобы попасть на предыдущую страницу,
                            // нам не нужно ничего вычислять
                            echo '<a href="'.$page_number.'">< </a> &nbsp; ';
                        }
                        // __________________ конец блока 1 __________________
                        // На данном этапе номер текущей страницы = $page_number+1
                        $current_page = $page_number + 1;
                        // Узнаем с какой ссылки начинать вывод
                        $start = $current_page - $limit;
                        // Узнаем номер последней ссылки для вывода
                        $end = $current_page + $limit;
                        // Выводим ссылки на все страницы
                        // Начальное число $j в нашем случае должно равнятся единице, а не нулю
                        for ($j = 1; $j < $recuired_pages; $j++)
                        {
                            // Выводим ссылки только в том случае, если их номер больше или равен
                            // начальному значению, и меньше или равен конечному значению
                            if ($j>=$start && $j<=$end)
                            {
                                // Ссылка на текущую страницу выделяется жирным
                                if ($j == ($page_number + 1))
                                {
                                    echo '<a href="'.$j.'"><strong style="color: #df0000">'.$j.'</strong></a> &nbsp; ';
                                }
                                // Ссылки на остальные страницы
                                else
                                {
                                    echo '<a href="'.$j.'">'.$j.'</a> &nbsp; ';
                                }
                            }
                        }
                        // _________________ начало блока 2 _________________
                        // Выводим ссылки "вперед" и "на последнюю страницу"
                        if ($j > $page_number && ($page_number + 2) < $j)
                        {
                            // Чтобы попасть на следующую страницу нужно увеличить $recuired_pages на 2
                            echo '<a href="'.($page_number + 2).'"> ></a> &nbsp; ';
                            // Так как у нас $j = количество страниц + 1, то теперь
                            // уменьшаем его на единицу и получаем ссылку на последнюю страницу
                            echo '<a href="'.($j - 1).'">>></a> &nbsp; ';
                        }
                        // __________________ конец блока 2 __________________
                     ?>
                </section>
                <div class="spacer-60"></div>
            </div>
        </div>
