<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?php echo $title ?></title>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url() ?>resources/css/bootstrap.css" rel="stylesheet">
        <!-- Owl Carousel Assets -->
        <link href="<?php echo base_url() ?>resources/css/owl.carousel.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>resources/css/owl.theme.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>resources/css/owl.transitions.css" rel="stylesheet">
        <!-- Flexslider CSS -->
        <link href="<?php echo base_url() ?>resources/css/flexslider.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="<?php echo base_url() ?>resources/css/main_style.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>resources/css/my_style.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="<?php echo base_url() ?>resources/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <!-- bxslider -->
        <link href="<?php echo base_url() ?>resources/css/jquery.bxslider.css" rel="stylesheet">
        <!-- jQuery -->
        <script src="<?php echo base_url() ?>resources/js/jquery.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo base_url() ?>resources/js/bootstrap.min.js"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <!-- Top Bar -->
        <section class="top_sec">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6 top_lft">
                        <div class="soc_ico">
                            <ul>
                                <li class="tweet">
                                    <a href="#">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li class="fb">
                                    <a href="#">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li class="insta">
                                    <a href="#">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                                <li class="linkd">
                                    <a href="#">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                </li>
                                <li class="ytube">
                                    <a href="#">
                                        <i class="fa fa-youtube"></i>
                                    </a>
                                </li>
                                <li class="rss">
                                    <a href="#">
                                        <i class="fa fa-rss"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="inf_txt">
                            <p>Luxury Real Estate Specialists Worldwide</p>
                        </div>
                    </div>
                    <!-- /.top-left -->
                        <?php
                            if(!isset($_SESSION['email']))
                            {
                                echo "<div class='col-xs-12 col-md-6 top_rgt margin_top_10'>";
                                echo "<div class='sig_in'>";
                                echo "<p>";
                                echo "<i class='fa fa-user'></i>";
                                echo "<a href='#login_box' class='log_btn' data-toggle='modal'> Sign in </a>";
                                echo "or";
                                echo "<a class='reg_btn' href='#reg_box' data-toggle='modal'> create account </a>";
                                echo "</p>";
                                echo "</div>";
                                echo "</div>";
                            }
                            else
                            {
                                echo "<div class='col-xs-12 col-md-6 top_rgt'>";
                                echo "<div class='inf_txt'>";
                                echo "<ul class='margin_bottom_0'>";
                                echo "<li class='li_inline'>";
                                echo "<p class='text-right'>".$_SESSION['email']."</p>";
                                echo "</li>";
                                echo "<li class='li_inline'>";
                                echo form_open("main/log_out_request", array("id" => "log_out_form"));
                                echo "<button class='btn btn-primary main_log_out' type='submit' name='log_out_button'>Log out</button>";
                                echo "</form>";
                                echo "</li>";
                                echo "</ul>";
                                echo "</div>";
                                echo "</div>";
                            }
                         ?>
                    <!-- /.top-right -->
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </section>





        <!-- Navigation -->
        <nav class="navbar" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Logo -->
                    <a class="navbar-brand" href="/"><img src="<?php echo base_url() ?>resources/images/logo.png" alt="logo"></a>
                </div>
                <!-- Navigation -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="/">Home</a>
                        </li>
                        <li>
                            <?php
                                $path1 = uri_string();
                                $pattern1 = '/property\/all\/[0-9]+/';
                                if(preg_match($pattern1, $path1) == TRUE)
                                {
                                    echo '<a href="1">Property Listings</a>';
                                }
                                else
                                {
                                    echo '<a href="/property/all/1">Property Listings</a>';
                                }
                            ?>
                        </li>
                        <li>
                            <?php
                                $path2 = uri_string();
                                $pattern2 = '/agents\/all\/[0-9]+/';
                                if(preg_match($pattern2, $path2) == TRUE)
                                {
                                    echo '<a href="1">Our Agents</a>';
                                }
                                else
                                {
                                    echo '<a href="/agents/all/1">Our Agents</a>';
                                }
                            ?>
                        </li>
                        <li>
                            <a href="/contact">Contact</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>
