    <!-- Footer -->
    <footer>
        <!-- Footer Top -->
        <div class="footer_top">
            <div class="container">
                <div class="row">
                    <!-- About Section -->
                    <div class="col-md-3 abt_sec">
                        <h2 class="foot_title">
                           About Wedoor
                        </h2>
                        <p>
                            Ethical quinoa slow-carb squid, irony Pitchfork tousled hella art party PBR&amp;B cray dreamcatcher brunch.
                        </p>
                        <div class="spacer-20"></div>
                        <p>
                            Bicycle rights jean shorts organic, street art PBR occupy flexitarian pour-over master cleanse farm-to-table.
                        </p>
                    </div>
                    <!-- Latest Tweets -->
                    <div class="col-md-3">
                        <h2 class="foot_title">
                           Latest Tweets
                        </h2>
                        <ul class="tweets">
                            <li>
                                <i class="fa fa-twitter"></i>
                                <p class="twee">
                                    Check out this great
                                    <a href="#">#themeforest</a>
                                    item 'Responsive Photography WordPress
                                    <a href="#">http://drbl.in/871942</a>
                                </p>
                                <p class="datd"> 6 April 2015 </p>
                                <div class="clearfix"></div>
                            </li>
                            <li class="spacer-20"></li>
                            <li>
                                <i class="fa fa-twitter"></i>
                                <p class="twee">
                                    <a href="#"> #MadeBySeries </a>
                                    Made By: Chris Coyier, Founder
                                    <a href="#">  http://ow.ly/LeAKf </a>
                                </p>
                                <p class="datd"> 6 April 2015 </p>
                                <div class="clearfix"></div>
                            </li>
                        </ul>
                    </div>
                    <!-- Contact Info -->
                    <div class="col-md-3">
                        <h2 class="foot_title">
                           Contact Info
                        </h2>
                        <ul class="cont_info">
                            <li>
                                <i class="fa fa-map-marker"></i>
                                <p>371 Linden Avenue Longwood, FL 32750 </p>
                            </li>
                            <li>
                                <i class="fa fa-phone"></i>
                                <p>
                                    <a href="tel:407-546-2034"> Phone: 407-546-2034 </a>
                                </p>
                            </li>
                            <li>
                                <i class="fa fa-envelope"></i>
                                <p>
                                    <a href="mailto:connect@crelegant.com?Subject=template%20enquiry"> Email: connect@crelegant.com </a>
                                </p>
                            </li>
                        </ul>
                    </div>
                    <!-- Useful Links -->
                    <div class="col-md-3">
                        <h2 class="foot_title">
                            Useful Links
                        </h2>
                        <ul class="foot_nav">
                            <li>
                                <a href="index.html">Home Search</a>
                            </li>
                            <li>
                                <a href="property_listing.html">Properties Inspection</a>
                            </li>
                            <li>
                                <a href="agents.html">Agents Consult</a>
                            </li>
                            <li>
                                <a href="blog.html">Latest News</a>
                            </li>
                            <li>
                                <a href="contact.html">Get in touch</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </div>
        <!-- Copyright -->
        <div class="footer_copy_right">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 text-left">
                        <p>
                            &copy; Copyright 2014. All Rights Reserved by
                            <a href="#"> WeDoor </a>
                        </p>
                    </div>
                    <div class="col-sm-6 text-right">
                        <p>Template developed by
                            <a href="http://themeforest.net/user/crelegant"> The Crelegant Team </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>



    <!-- Modal HTML -->
    <div id="login_box" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="log_form">
                        <h2 class="frm_titl"> Login Form </h2>
                        <?php echo form_open("main/log_in_request", array("id" => "log_in_form")) ?>
                            <div class="control-group form-group">
                                <div class="form-group controls">
                                    <input name="username_log_in" type="text" class="form-control" id="username_log_in" placeholder="Username">
                                    <p class="help-block"></p>
                                </div>
                                <div class="form-group controls">
                                    <input name="password_log_in" type="password" class="form-control" id="password_log_in" placeholder="Password">
                                    <p class="help-block"></p>
                                </div>
                                <div class="clearfix"></div>
                                <button name="log_in_button" type="submit" class="btn btn-primary">Sign In</button>
                                <!-- For success/fail messages -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <div id="reg_box" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="log_form">
                        <h2 class="frm_titl"> Create Account </h2>
                        <?php echo form_open("main/registration_request", array("id" => "registration_form")) ?>
                            <div class="control-group form-group">
                                <div class="form-group controls">
                                    <input type="text" class="form-control" id="username-reg" placeholder="Username" name="username-reg">
                                    <p class="help-block"></p>
                                </div>
                                <div class="form-group controls">
                                    <input data-toggle="tooltip" type="text" class="form-control" id="email-reg" placeholder="Email" name="email-reg" title="Email can't be longer than 35 characters. Can contain only Latin characters, numbers and symbols (. - _)" data-placement="top">
                                    <p class="help-block"></p>
                                </div>
                                <div class="form-group controls">
                                    <input type="password" class="form-control" id="password-reg" placeholder="Password" name="password-reg">
                                    <p class="help-block"></p>
                                </div>
                                <div class="form-group controls">
                                    <input type="password" class="form-control" id="confirm_password" placeholder="Confirm Password" name="confirm_password">
                                    <p class="help-block"></p>
                                </div>
                                <button type="submit" class="btn btn-primary" name="new_user_button">Create Account</button>
                                <!-- For success/fail messages -->
                            </div>
                            <div id="eemail"></div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>








<script>
    $(document).ready(function ()
    {
        $('[data-toggle="tooltip"]').tooltip();
        $('#registration_form').submit(function(e)
        {
            e.preventDefault();
            var me = $(this);
            $.ajax(
            {
                url: me.attr('action'),
                type: 'POST',
                data: me.serialize(),
                dataType: 'json',
                success: function(response)
                {
                    if(response.success == true)
                    {
                        $('.form-group')
                        .removeClass('has-error')
                        .removeClass('has-success');
                        $('.text-danger').remove();
                        me[0].reset();
                        location.reload();
                    }
                    else
                    {
                        $.each(response.messages, function(key, value)
                        {
                            var element = $('#' + key);
                            element.closest('div.form-group')
                            .removeClass('has-error')
                            .addClass(value.length > 0 ? 'has-error' : 'has-success')
                            .find('.text-danger')
                            .remove();
                            element.after(value);
                        });
                    }
                }
            });
        });
        $('#log_in_form').submit(function(e)
        {
            e.preventDefault();
            var me = $(this);
            $.ajax(
            {
                url: me.attr('action'),
                type: 'POST',
                data: me.serialize(),
                dataType: 'json',
                success: function(response)
                {
                    if(response.success == true)
                    {
                        $('.form-group')
                        .removeClass('has-error')
                        .removeClass('has-success');
                        $('.text-danger').remove();
                        me[0].reset();
                        location.reload();
                    }
                    else
                    {
                        $.each(response.messages, function(key, value)
                        {
                            var element = $('#' + key);
                            element.closest('div.form-group')
                            .removeClass('has-error')
                            .addClass(value.length > 0 ? 'has-error' : 'has-success')
                            .find('.text-danger')
                            .remove();
                            element.after(value);
                        });
                    }
                }
            });
        });
        $('#log_out_form').submit(function(e)
        {
            e.preventDefault();
            var me = $(this);
            $.ajax(
            {
                url: me.attr('action'),
                type: 'POST',
                success: function()
                {
                    location.reload();
                }
            });
        });
    });
</script>
</body>
</html>
