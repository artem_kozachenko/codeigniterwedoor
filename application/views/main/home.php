        <!-- Header Stat Banner -->
        <header id="banner" class="stat_bann">
            <div class="bannr_sec">
                <img src="http://placehold.it/1350x900" alt="Banner">
                <h1 class="main_titl">
                    Best Real Estate Deals site
                </h1>
                <h4 class="sub_titl">
                    Wes Anderson American Apparel
                </h4>
            </div>
        </header>





        <!-- Page Content -->
        <section id="srch_slide">
            <div class="container">
                <!-- Search & Slider -->
                <div class="row">



                    <!-- Search Form -->
                    <div class="col-md-4">
                        <div class="srch_frm">
                            <h3>Real Estate Search</h3>
                            <?php echo form_open('property/all/1') ?>
                                <div class="control-group form-group">
                                    <div class="controls">
                                        <label>Location </label>
                                        <select id="city" name="city" class="form-control">
                                            <option value="">Choose location</option>
                                            <option value="Alabama">Alabama</option>
                                            <option value="Alaska">Alaska</option>
                                            <option value="Arizona">Arizona</option>
                                            <option value="Arkansas">Arkansas</option>
                                            <option value="California">California</option>
                                            <option value="Colorado">Colorado</option>
                                            <option value="Connecticut">Connecticut</option>
                                            <option value="Delaware">Delaware</option>
                                            <option value="District Of Columbia">District Of Columbia</option>
                                            <option value="Florida">Florida</option>
                                            <option value="Georgia">Georgia</option>
                                            <option value="Hawaii">Hawaii</option>
                                            <option value="Idaho">Idaho</option>
                                            <option value="Illinois">Illinois</option>
                                            <option value="Indiana">Indiana</option>
                                            <option value="Iowa">Iowa</option>
                                            <option value="Kansas">Kansas</option>
                                            <option value="Kentucky">Kentucky</option>
                                            <option value="Louisiana">Louisiana</option>
                                            <option value="Maine">Maine</option>
                                            <option value="Maryland">Maryland</option>
                                            <option value="Massachusetts">Massachusetts</option>
                                            <option value="Michigan">Michigan</option>
                                            <option value="Minnesota">Minnesota</option>
                                            <option value="Mississippi">Mississippi</option>
                                            <option value="Missouri">Missouri</option>
                                            <option value="Montana">Montana</option>
                                            <option value="Nebraska">Nebraska</option>
                                            <option value="Nevada">Nevada</option>
                                            <option value="New Hampshire">New Hampshire</option>
                                            <option value="New Jersey">New Jersey</option>
                                            <option value="New Mexico">New Mexico</option>
                                            <option value="New York">New York</option>
                                            <option value="North Carolina">North Carolina</option>
                                            <option value="North Dakota">North Dakota</option>
                                            <option value="Ohio">Ohio</option>
                                            <option value="Oklahoma">Oklahoma</option>
                                            <option value="Oregon">Oregon</option>
                                            <option value="Pennsylvania">Pennsylvania</option>
                                            <option value="Rhode Island">Rhode Island</option>
                                            <option value="South Carolina">South Carolina</option>
                                            <option value="South Dakota">South Dakota</option>
                                            <option value="Tennessee">Tennessee</option>
                                            <option value="Texas">Texas</option>
                                            <option value="Utah">Utah</option>
                                            <option value="Vermont">Vermont</option>
                                            <option value="Virginia">Virginia</option>
                                            <option value="Washington">Washington</option>
                                            <option value="West Virginia">West Virginia</option>
                                            <option value="Wisconsin">Wisconsin</option>
                                            <option value="Wyoming">Wyoming</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="control-group form-group">
                                    <div class="controls col-md-6 first">
                                        <label>Bedrooms</label>
                                        <input id="bedrooms" class="form-control" type="text" name="bedroom" placeholder="Enter quantity">
                                    </div>
                                    <div class="controls col-md-6">
                                        <label>Type </label>
                                        <select id="type" name="type" class="form-control">
                                            <option value="">Choose type</option>
                                            <option value="Rent">For Rent</option>
                                            <option value="Sale">For Sale</option>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="control-group form-group">
                                    <div class="controls col-md-6 first">
                                        <label>Min. Price </label>
                                        <select id="min_price" name="min_price" class="form-control">
                                            <option value="">Min. price</option>
                                            <option value="50">$50</option>
                                            <option value="100">$100</option>
                                            <option value="200">$200</option>
                                            <option value="300">$300</option>
                                            <option value="400">$400</option>
                                            <option value="500">$500</option>
                                            <option value="600">$600</option>
                                            <option value="700">$700</option>
                                            <option value="800">$800</option>
                                            <option value="900">$900</option>
                                            <option value="1000">$1000</option>
                                            <option value="1500">$1500</option>
                                            <option value="2000">$2000</option>
                                            <option value="2500">$2500</option>
                                        </select>
                                    </div>
                                    <div class="controls col-md-6">
                                        <label>Max. Price </label>
                                        <select id="max_price" name="max_price" class="form-control">
                                            <option value=""> Max. price</option>
                                            <option value="100">$100</option>
                                            <option value="200">$200</option>
                                            <option value="300">$300</option>
                                            <option value="400">$400</option>
                                            <option value="500">$500</option>
                                            <option value="600">$600</option>
                                            <option value="700">$700</option>
                                            <option value="800">$800</option>
                                            <option value="900">$900</option>
                                            <option value="1000">$1000</option>
                                            <option value="1500">$1500</option>
                                            <option value="2000">$2000</option>
                                            <option value="2500">$2500</option>
                                            <option value="3000">$3000</option>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="control-group form-group">
                                    <div class="controls col-md-6 first">
                                        <label>Garages</label>
                                        <input id="garages" class="form-control" type="text" name="garage" placeholder="Enter quantity">
                                    </div>
                                    <!-- <div class="clearfix"></div> -->
                                </div>
                                <div class="control-group form-group">
                                    <div class="controls col-md-6 first">
                                        <label>Bathrooms</label>
                                        <input id="bathrooms" class="form-control" type="text" name="bathroom" placeholder="Enter quantity">
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <!-- For success/fail messages -->
                                <button name="search_button" type="submit" class="btn btn-primary">Search</button>
                            </form>
                        </div>
                    </div>



                     <!-- Slider -->
                    <div class="col-md-8 slide_sec slider_height_463">
                        <div id="slider" class="silde_img flexslider">
                            <ul class="slides">
                                <?php
                                    $property_count = $this->db->count_all_results('property');
                                    $random_position = rand(1, ($property_count - 5));
                                    $query = $this->get_data->get_5_property($random_position);
                                    foreach($query->result_array() as $row)
                                    {
                                        $description = $row['description'];
                                        $description = substr($description, 0, 85);
                                        $description .= '...';
                                        echo "<li>";
                                        echo "<img class='img-responsive img-hover property_img' src='".$row['photo_1']."' alt='Slider image' />";
                                        echo "<div class='slide-info'>";
                                        echo "<p class='sli_price'> $".$row['price']." </p>";
                                        echo "<p class='sli_titl'> <a href='/property/id/".$row['property_id']."'>".$row['name']."</a> </p>";
                                        echo "<p class='sli_desc'> ".$description." </p>";
                                        echo "</div>";
                                        echo "</li>";
                                    }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </section>





        <div class="spacer-60"></div>




        <!-- Featured Properties Section -->
        <section id="feat_propty">
            <div class="container">
                <div class="row">
                    <div class="titl_sec">
                        <div class="col-xs-6">
                            <h3 class="main_titl text-left">
                                Featured Properties
                            </h3>
                        </div>
                        <div class="col-xs-6">
                            <h3 class="link_titl text-right">
                                <a href="/property/all/1"> View Properties </a>
                            </h3>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <?php
                        $property_count = $this->db->count_all_results('property');
                        $random_position = rand(1, ($property_count - 3));
                        $query = $this->get_data->get_3_property($random_position);
                        foreach($query->result_array() as $row)
                        {
                            $description = $row['description'];
                            $description = substr($description, 0, 82);
                            $description .= '...';
                            echo "<div class='col-md-4'>";
                            echo "<div class='panel panel-default'>";
                            echo "<div class='panel-image'>";
                            echo "<img class='img-responsive img-hover' src='".$row['photo_1']."'>";
                            echo "<div class='img_hov_eff'>";
                            echo "<a class='btn btn-default btn_trans' href='/property/id/".$row['property_id']."'> More Details </a>";
                            echo "</div>";
                            echo "</div>";
                            echo "<div class='sal_labl'>For ".$row['type']."</div>";
                            echo "<div class='panel-body'>";
                            echo "<div class='prop_feat'>";
                            echo "<p class='area'><i class='fa fa-home'></i> ".$row['area']." Sq Ft</p>";
                            echo "<p class='bedrom'><i class='fa fa-bed'></i> ".$row['bedroom']." Bedrooms</p>";
                            echo "<p class='bedrom'><i class='fa fa-car'></i> ".$row['garage']." Garage</p>";
                            echo "</div>";
                            echo "<h3 class='sec_titl'>".$row['name']."</h3>";
                            echo "<div class='description_div'>";
                            echo "<p class='sec_desc'>".$description."</p>";
                            echo "</div>";
                            echo "<div class='panel_bottom'>";
                            echo "<div class='col-md-12'>";
                            echo "<p class='price text-left'> $".$row['price']."</p>";
                            echo "</div>";
                            echo "</div>";
                            echo "</div>";
                            echo "</div>";
                            echo "</div>";
                        }
                    ?>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </section>





        <div class="spacer-60"></div>





        <!-- Talented Agents Section -->
        <section id="talen_agent">
            <div class="container">
                <div class="row">
                    <div class="titl_sec">
                        <div class="col-xs-6">
                            <h3 class="main_titl text-left">
                                talented agents
                            </h3>
                        </div>
                        <div class="col-xs-6">
                            <h3 class="link_titl text-right">
                                <a href="/agents/all/1"> all agents </a>
                            </h3>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <?php
                        $agents_count = $this->db->count_all_results('agents');
                        $random_position = rand(1, ($agents_count - 4));
                        $query = $this->get_data->get_4_agents($random_position);
                        foreach($query->result_array() as $row)
                        {
                            echo "<div class='col-md-3'>";
                            echo "<div class='panel panel-default'>";
                            echo "<div class='panel-image'>";
                            echo "<img class='img-responsive img-hover talented_agent_photo' src='".$row['photo']."'>";
                            echo "</div>";
                            echo "<div class='panel-body'>";
                            echo "<h3 class='sec_titl text-center'>";
                            echo "<a href='/agents/id/".$row['agent_id']."'>".$row['first_name']." ".$row['second_name']."</a>";
                            echo "</h3>";
                            echo "<p class='sec_desc text-center'>Buying Agents</p>";
                            echo "<div class='panel_hidd'>";
                            echo "<hr>";
                            echo "<p class='phon text-center'>";
                            echo "<a href='tel:253-891-8159'> Phone: ".$row['phone_number']." </a>";
                            echo "</p>";
                            echo "</div>";
                            echo "</div>";
                            echo "</div>";
                            echo "</div>";
                        }
                        ?>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </section>





        <div class="spacer-60"></div>





        <!-- Owl Carousel JavaScript -->
        <script src="<?php echo base_url() ?>resources/js/owl.carousel.min.js"></script>
        <!-- Flexslider JavaScript -->
        <script src="<?php echo base_url() ?>resources/js/jquery.flexslider-min.js"></script>
        <!-- Script to Activate the Carousels -->
        <script type="text/javascript">
            $(document).ready(function () {
                'use strict';
                $("#owl-carousel").owlCarousel({
                    items: 5,
                    itemsDesktop: [1199, 5],
                    itemsDesktopSmall: [979, 3],
                    itemsTablet: [768, 2],
                    itemsMobile: [479, 1],
                    navigation: true,
                    navigationText: [
                        "<i class='fa fa-chevron-left icon-white'></i>",
                        "<i class='fa fa-chevron-right icon-white'></i>"
                    ],
                    autoPlay: false,
                    pagination: false
                });
                $("#slide_pan").owlCarousel({
                    items: 1,
                    itemsDesktop: [1199, 1],
                    itemsDesktopSmall: [979, 1],
                    itemsTablet: [768, 1],
                    itemsMobile: [479, 1],
                    navigation: true,
                    navigationText: [
                        "<i class='fa fa-chevron-left icon-white'></i>",
                        "<i class='fa fa-chevron-right icon-white'></i>"
                    ],
                    autoPlay: false,
                    pagination: false
                });
                $(".testim_sec").owlCarousel({
                    items: 2,
                    itemsDesktop: [1199, 2],
                    itemsDesktopSmall: [979, 2],
                    itemsTablet: [768, 1],
                    itemsMobile: [479, 1],
                    navigation: true,
                    navigationText: [
                        "<i class='fa fa-chevron-left icon-white'></i>",
                        "<i class='fa fa-chevron-right icon-white'></i>"
                    ],
                    autoPlay: false,
                    pagination: false
                });
                $('#slider').flexslider({
                    animation: "slide",
                    controlNav: false,
                    animationLoop: false,
                    slideshow: false
                });
            });
        </script>
