        <!-- Header bradcrumb -->
        <header class="bread_crumb">
            <div class="pag_titl_sec">
                <h1 class="pag_titl"> Contact Us </h1>
                <h4 class="sub_titl"> Echo Park occupy mustache gastropub </h4>
            </div>
        </header>





        <div class="spacer-60"></div>





        <div class="container">
            <div class="row">
                <!-- Contact Section -->
                <section id="contact_sec" class="col-md-8">
                    <!-- Contact form -->
                    <div class="row">
                        <div class="titl_sec">
                            <div class="col-lg-12">
                                <h3 class="main_titl text-left">
                                    Send us email
                                </h3>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-md-12">
                            <div class="cont_frm">
                                <?php echo form_open("main/send_message_request", array("id" => "contact_form")) ?>
                                <div id="unsuccess" class="col-md-12 text-danger text-center"></div>
                                <div id="success" class="col-md-12 text-success text-center"></div>
                                    <div class="control-group form-group col-md-6 first">
                                        <div class="form-group controls">
                                            <input name="name" type="text" class="form-control" id="name" placeholder="Your Name">
                                            <div class="in_ico">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <p class="help-block"></p>
                                        </div>
                                        <div class="form-group controls">
                                            <input data-toggle="tooltip" name="email" type="text" class="form-control" id="email" placeholder="Email Address" title="Email can't be longer than 35 characters. Can contain only Latin characters, numbers and symbols (. - _)" data-placement="top">
                                            <div class="in_ico">
                                                <i class="fa fa-envelope-o"></i>
                                            </div>
                                            <p class="help-block"></p>
                                        </div>
                                        <div class="form-group controls">
                                            <input name="phone_number" type="text" class="form-control" id="phone_number" placeholder="Your Phone">
                                            <div class="in_ico">
                                                <i class="fa fa-phone"></i>
                                            </div>
                                            <p class="help-block"></p>
                                        </div>
                                        <div class="form-group controls last">
                                            <input name="website" type="text" class="form-control" id="website" placeholder="Website">
                                            <div class="in_ico">
                                                <i class="fa fa-pencil"></i>
                                            </div>
                                            <p class="help-block"></p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="control-group form-group col-md-6">
                                        <div class="controls">
                                            <textarea name="message" class="form-control" id="message" style="resize:none" placeholder="Message"></textarea>
                                        </div>
                                        <button name="send_message_button" type="submit" class="btn btn-primary">Send Message</button>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div id="text-success"></div>
                                    <!-- For success/fail messages -->
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="spacer-30"></div>
                </section>
                <!-- Sidebar Section -->
                <section id="sidebar" class="col-md-4">
                    <!-- Contact Information -->
                    <div class="row">
                        <div class="titl_sec">
                            <div class="col-lg-12">
                                <h3 class="main_titl text-left">
                                    Weedor information
                                </h3>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="cont_info">
                            <div class="info_sec first">
                                <div class="icon_box">
                                    <i class="fa fa-map-marker"></i>
                                </div>
                                <p class="infos">4737 Horizon Circle Seattle WA 98109</p>
                            </div>
                            <div class="info_sec">
                                <div class="icon_box">
                                    <i class="fa fa-envelope-o"></i>
                                </div>
                                <p class="infos">
                                    <a href="mailto:weedor@realestate.com?Subject=template%20enquiry"> weedor@realestate.com </a>
                                </p>
                            </div>
                            <div class="info_sec">
                                <div class="icon_box">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <p class="infos">
                                    <a href="tel:+112-510-360-7617"> +112 (510) 360 7617 </a>
                                </p>
                            </div>
                            <div class="info_sec">
                                <div class="icon_box">
                                    <i class="fa fa-facebook"></i>
                                </div>
                                <p class="infos">facebook.com/weedor</p>
                            </div>

                        </div>
                    </div>
                    <!-- /.row -->
                </section>
                <div class="spacer-60"></div>
            </div>
        </div>
        <script>
        $(document).ready (function ()
        {
            $('[data-toggle="tooltip"]').tooltip();
            $('#contact_form').submit(function(e)
            {
                e.preventDefault();
                var me = $(this);
                $.ajax(
                {
                    url: me.attr('action'),
                    type: 'POST',
                    data: me.serialize(),
                    dataType: 'json',
                    success: function(response)
                    {
                        if(response.success == true)
                        {
                            $('.form-group')
                            .removeClass('has-error')
                            .removeClass('has-success');
                            $('.text-danger').remove();
                            me[0].reset();
                            if(response.delivered == true)
                            {
                                $('#success').text(response.messages.success);
                            }
                            else
                            {
                                $('#unsuccess').text(response.messages.unsuccess);
                            }
                        }
                        else
                        {
                            $.each(response.messages, function(key, value)
                            {
                                $('#unsuccess').remove();
                                $('#success').remove();
                                var element = $('#' + key);
                                element.closest('div.form-group')
                                .removeClass('has-error')
                                .addClass(value.length > 0 ? 'has-error' : 'has-success')
                                .find('.text-danger')
                                .remove();
                                element.after(value);
                            });
                        }
                    }
                });
            });
        });
        </script>
