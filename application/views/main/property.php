<?php
    $query = $this->get_data->one_property($property_id);
    foreach($query->result_array() as $row)
    {
        $name = $row['name'];
        $price = $row['price'];
        $type = $row['type'];
        $city = $row['city'];
        $area = $row['area'];
        $garage = $row['garage'];
        $bedroom = $row['bedroom'];
        $bathroom = $row['bathroom'];
        $acres = $row['acres'];
        $heat = $row['heat'];
        $dimensions = $row['dimensions'];
        $size_source = $row['size_source'];
        $ac = $row['ac'];
        for($i=1; $i <= 5; $i++)
        {
            $photo[$i] = $row['photo_'.$i];
        }
        $description = $row['description'];
    }
?>
    <!-- Header bradcrumb -->
    <header class="bread_crumb">
        <div class="pag_titl_sec">
            <h1 class="pag_titl"> Property Details </h1>
            <h4 class="sub_titl"> Echo Park occupy mustache gastropub </h4>
        </div>
    </header>





    <div class="spacer-60"></div>





    <div class="container">
        <div class="row">
            <!-- Proerty Details Section -->
            <section id="prop_detal" class="col-md-12">
                <div class="row">
                    <div class="panel panel-default">
                       <!-- Proerty Slider Images -->
                        <div class="panel-image">
                            <ul id="prop_slid">
                                <?php
                                for($i=1; $i <= 5; $i++)
                                {
                                    if($photo[$i] != '')
                                    {
                                        echo "<li>";
                                        echo "<img class='img-responsive img_size' src='".$photo[$i]."' alt='Property Slide Image'>";
                                        echo "</li>";
                                    }
                                }
                                ?>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <h3 class="sec_titl">
                                 <?php echo $name;?>
                             </h3>
                            <div class="col_labls larg_labl">
                                <p class="or_labl">For <?php echo $type;?></p>
                                <p class="blu_labl"> $<?php echo $price;?></p>
                            </div>
                            <p class="sec_desc">
                                <?php echo $description;?>
                            </p>
                            <!-- Proerty Additional Info -->
                            <div class="prop_addinfo">
                                <h2 class="add_titl">
                                Additional Details
                            </h2>
                                <div class="info_sec first">
                                    <div class="col-md-5">
                                        <ul>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-long-arrow-right"></i>
                                                    <p class="infos">
                                                        Price:
                                                        <span> $<?php echo $price;?> </span>
                                                    </p>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-long-arrow-right"></i>
                                                    <p class="infos">
                                                        For Sale/Rent:
                                                        <span> <?php echo $type;?> </span>
                                                    </p>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-long-arrow-right"></i>
                                                    <p class="infos">
                                                        Address:
                                                        <span> <?php echo $city;?> </span>
                                                    </p>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-long-arrow-right"></i>
                                                    <p class="infos">
                                                        Area:
                                                        <span> <?php echo $area;?> Sq Ft </span>
                                                    </p>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3">
                                        <ul>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-long-arrow-right"></i>
                                                    <p class="infos">
                                                        Garages:
                                                        <span> <?php echo $garage;?> </span>
                                                    </p>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-long-arrow-right"></i>
                                                    <p class="infos">
                                                        Bedrooms:
                                                        <span> <?php echo $bedroom;?> </span>
                                                    </p>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-long-arrow-right"></i>
                                                    <p class="infos">
                                                        Bathrooms:
                                                        <span> <?php echo $bathroom;?> </span>
                                                    </p>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-long-arrow-right"></i>
                                                    <p class="infos">
                                                        Acres:
                                                        <span> <?php echo $acres;?> </span>
                                                    </p>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4">
                                        <ul>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-long-arrow-right"></i>
                                                    <p class="infos">
                                                        Heat:
                                                        <span> <?php echo $heat;?> </span>
                                                    </p>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-long-arrow-right"></i>
                                                    <p class="infos">
                                                        Dimensions:
                                                        <span> <?php echo $dimensions;?> </span>
                                                    </p>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-long-arrow-right"></i>
                                                    <p class="infos">
                                                        Size Source:
                                                        <span> <?php echo $size_source;?> </span>
                                                    </p>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-long-arrow-right"></i>
                                                    <p class="infos">
                                                        AC:
                                                        <span> <?php echo $ac;?> </span>
                                                    </p>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <div class="spacer-30"></div>
                <?php
                    $agent_count = $this->db->count_all_results('agents');
                    $random_position = rand(1, $agent_count);
                    $query = $this->get_data->random_agent($random_position);
                    foreach($query->result_array() as $row)
                    {
                        $first_name = $row['first_name'];
                        $second_name = $row['second_name'];
                        $photo = $row['photo'];
                        $phone_number = $row['phone_number'];
                        $email = $row['email'];
                        $skype = $row['skype'];
                        $about = $row['about'];
                    }
                    $about = substr($about, 0, 200);
                    $about .= '...';
                ?>
                <!-- Agent Info -->
                <div class="row">
                    <div class="agen_info">
                        <div class="col-md-4">
                            <a href="agents_single.html">
                                <img class="img-responsive img-hover agent_photo" src="<?php echo $photo?>" alt="">
                            </a>
                        </div>
                        <div class="col-md-8">
                            <div class="panel panel-default agent_div">
                                <div class="panel-body">
                                    <div class="row agen_desc">
                                        <div class="col-sm-8">
                                            <h3 class="sec_titl">
                                                <a href="agents_single.html"> <?php echo $first_name.' '.$second_name;?> </a>
                                            </h3>
                                            <p class="sec_desc">
                                                Buying Agents
                                            </p>
                                        </div>
                                    </div>
                                    <div class='about_height'>
                                        <p class="sec_desc">
                                            <?php echo $about;?>
                                        </p>
                                    </div>
                                    <div class="panel_bottom ">
                                        <div class="agen_feat panel_bottom_info">
                                            <p class="area">
                                                <a href="tel:910-213-7890">
                                                    <i class="fa fa-phone"></i>
                                                    <?php echo $phone_number;?>
                                                </a>
                                            </p>
                                            <p class="bedrom">
                                                <a href="#">
                                                    <i class="fa fa-envelope"></i>
                                                    <?php echo $email;?>
                                                </a>
                                            </p>
                                            <p class="bedrom">
                                                <a href="skype:-scottberends1-?chat">
                                                    <i class="fa fa-skype"></i>
                                                    <?php echo $skype;?>
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </section>















            <div class="spacer-60"></div>

        </div>
    </div>





    <!-- BX Slider -->
    <script src="<?php echo base_url() ?>resources/js/jquery.bxslider.min.js"></script>
    <!-- Script to Activate the Carousel -->
    <script>
        /* Product Slider Codes */
        $(document).ready(function () {
        'use strict';
        $('#prop_slid').bxSlider({
            pagerCustom: '#slid_nav'
        });
    });
    </script>
