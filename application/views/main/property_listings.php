    <!-- Header bradcrumb -->
    <header class="bread_crumb">
        <div class="pag_titl_sec">
            <h1 class="pag_titl"> Property Listing </h1>
            <h4 class="sub_titl"> Echo Park occupy mustache gastropub </h4>
        </div>
    </header>




    <div class="spacer-60"></div>


    <div class="container">
        <div class="row">
            <!-- Properties Section -->
            <section id="feat_propty">
                <div class="container">
                    <?php
                        // Если значение page= не является числом, то показываем
                        // пользователю первую страницу
                        if(!is_numeric($page_number)) $page_number = 1;
                        // Узнаем количество всех доступных записей
                        if(!isset($_SESSION['post']))
                        {
                            $property_count = $this->db->count_all_results('property');
                        }
                        else if ($_SESSION['post'] == 1)
                        {
                            $property_count = 0;
                        }
                        else if(isset($_SESSION['post']['min_price']) && !isset($_SESSION['post']['max_price']))
                        {
                            $this->db->where('price >=', $_SESSION['post']['min_price']);
                            $property_count = $this->db->count_all_results('property');
                        }
                        else if(isset($_SESSION['post']['max_price']) && !isset($_SESSION['post']['min_price']))
                        {
                            $this->db->where('price <=', $_SESSION['post']['max_price']);
                            $property_count = $this->db->count_all_results('property');
                        }
                        else if(isset($_SESSION['post']['min_price']) && isset($_SESSION['post']['max_price']))
                        {
                            $where = 'price BETWEEN '.$_SESSION['post']['min_price'].' AND '.$_SESSION['post']['max_price'].'';
                            $this->db->where($where);
                            $property_count = $this->db->count_all_results('property');
                        }
                        else
                        {
                            $this->db->where($_SESSION['post']);
                            $property_count = $this->db->count_all_results('property');
                        }
                        if ($property_count != 0) {
                            // Устанавливаем количество записей, которые будут выводиться на одной странице
                            // Поставьте нужное вам число. Для примера я указал одну запись на страницу
                            $property_limit = 8;
                            // Ограничиваем количество ссылок, которые будут выводиться перед и
                            // после текущей страницы$page_number
                            $limit = 3;
                            // Вычисляем количество страниц, чтобы знать сколько ссылок выводить
                            $recuired_pages = $property_count / $property_limit;
                            // Округляем полученное число страниц в большую сторону
                            $recuired_pages = ceil($recuired_pages);
                            // Если пользователь вручную поменяет в адресной строке значение page= на нуль или больше $required_pages,
                            // то мы определим это и поменяем на единицу, то-есть отправим на первую
                            // страницу, чтобы избежать ошибки
                            if ($page_number < 1 OR $page_number > $recuired_pages)
                            {
                                redirect('property/all/1');
                            }
                            // Здесь мы увеличиваем число страниц на единицу чтобы начальное значение было
                            // равно единице, а не нулю. Значение page_number= будет
                            // совпадать с цифрой в ссылке, которую будут видеть посетители
                            $recuired_pages++;
                            // Если значение page_number= больше числа страниц, то выводим первую страницу
                            if ($page_number > $recuired_pages) $page_number = 1;
                            // Переменная $list указывает с какой записи начинать выводить данные.
                            // Если это число не определено, то будем выводить
                            // с самого начала, то-есть с нулевой записи
                            if (!isset($list)) $list = 0;
                            // Чтобы у нас значение page_number= в адресе ссылки совпадало с номером
                            // страницы мы будем его увеличивать на единицу при выводе ссылок, а
                            // здесь наоборот уменьшаем чтобы ничего не нарушить.
                            $list = --$page_number * $property_limit;
                            if($_SESSION['post'] != '')
                            {
                                $query = $this->get_data->search_property($property_limit, $list, $_SESSION['post']);
                            }
                            else
                            {
                                $query = $this->get_data->all_property($property_limit, $list);
                            }
                            // Делаем запрос подставляя значения переменных $property_limit и $list
                            $property_left = count($query->result_array());
                            $counter = 0;
                            $first_row = 1;
                            foreach ($query->result_array() as $row)
                            {
                                $description = $row['description'];
                                $description = substr($description, 0, 82);
                                $description .= '...';
                                if($counter == 0 && $first_row == 0)
                                {
                                    echo "<div class='row'>";
                                    echo "<div class='col-md-4'>";
                                }
                                else if($counter == 0 && $first_row == 1)
                                {
                                    echo "<div class='row'>";
                                    ?>
                                    <!-- Search Form -->
                                    <div class="col-md-4">
                                        <div class="srch_frm">
                                            <h3>Real Estate Search</h3>
                                            <?php echo form_open('property/all/1') ?>
                                                <div class="control-group form-group">
                                                    <div class="controls">
                                                        <label>Location </label>
                                                        <select id="city" name="city" class="form-control">
                                                            <option value="">Choose location</option>
                                                            <option value="Alabama">Alabama</option>
                                                            <option value="Alaska">Alaska</option>
                                                            <option value="Arizona">Arizona</option>
                                                            <option value="Arkansas">Arkansas</option>
                                                            <option value="California">California</option>
                                                            <option value="Colorado">Colorado</option>
                                                            <option value="Connecticut">Connecticut</option>
                                                            <option value="Delaware">Delaware</option>
                                                            <option value="District Of Columbia">District Of Columbia</option>
                                                            <option value="Florida">Florida</option>
                                                            <option value="Georgia">Georgia</option>
                                                            <option value="Hawaii">Hawaii</option>
                                                            <option value="Idaho">Idaho</option>
                                                            <option value="Illinois">Illinois</option>
                                                            <option value="Indiana">Indiana</option>
                                                            <option value="Iowa">Iowa</option>
                                                            <option value="Kansas">Kansas</option>
                                                            <option value="Kentucky">Kentucky</option>
                                                            <option value="Louisiana">Louisiana</option>
                                                            <option value="Maine">Maine</option>
                                                            <option value="Maryland">Maryland</option>
                                                            <option value="Massachusetts">Massachusetts</option>
                                                            <option value="Michigan">Michigan</option>
                                                            <option value="Minnesota">Minnesota</option>
                                                            <option value="Mississippi">Mississippi</option>
                                                            <option value="Missouri">Missouri</option>
                                                            <option value="Montana">Montana</option>
                                                            <option value="Nebraska">Nebraska</option>
                                                            <option value="Nevada">Nevada</option>
                                                            <option value="New Hampshire">New Hampshire</option>
                                                            <option value="New Jersey">New Jersey</option>
                                                            <option value="New Mexico">New Mexico</option>
                                                            <option value="New York">New York</option>
                                                            <option value="North Carolina">North Carolina</option>
                                                            <option value="North Dakota">North Dakota</option>
                                                            <option value="Ohio">Ohio</option>
                                                            <option value="Oklahoma">Oklahoma</option>
                                                            <option value="Oregon">Oregon</option>
                                                            <option value="Pennsylvania">Pennsylvania</option>
                                                            <option value="Rhode Island">Rhode Island</option>
                                                            <option value="South Carolina">South Carolina</option>
                                                            <option value="South Dakota">South Dakota</option>
                                                            <option value="Tennessee">Tennessee</option>
                                                            <option value="Texas">Texas</option>
                                                            <option value="Utah">Utah</option>
                                                            <option value="Vermont">Vermont</option>
                                                            <option value="Virginia">Virginia</option>
                                                            <option value="Washington">Washington</option>
                                                            <option value="West Virginia">West Virginia</option>
                                                            <option value="Wisconsin">Wisconsin</option>
                                                            <option value="Wyoming">Wyoming</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="control-group form-group">
                                                    <div class="controls col-md-6 first">
                                                        <label>Bedrooms</label>
                                                        <input id="bedrooms" class="form-control" type="text" name="bedroom" placeholder="Enter quantity">
                                                    </div>
                                                    <div class="controls col-md-6">
                                                        <label>Type </label>
                                                        <select id="type" name="type" class="form-control">
                                                            <option value="">Choose type</option>
                                                            <option value="Rent">For Rent</option>
                                                            <option value="Sale">For Sale</option>
                                                        </select>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="control-group form-group">
                                                    <div class="controls col-md-6 first">
                                                        <label>Min. Price </label>
                                                        <select id="min_price" name="min_price" class="form-control">
                                                            <option value="">Min. price</option>
                                                            <option value="50">$50</option>
                                                            <option value="100">$100</option>
                                                            <option value="200">$200</option>
                                                            <option value="300">$300</option>
                                                            <option value="400">$400</option>
                                                            <option value="500">$500</option>
                                                            <option value="600">$600</option>
                                                            <option value="700">$700</option>
                                                            <option value="800">$800</option>
                                                            <option value="900">$900</option>
                                                            <option value="1000">$1000</option>
                                                            <option value="1500">$1500</option>
                                                            <option value="2000">$2000</option>
                                                            <option value="2500">$2500</option>
                                                        </select>
                                                    </div>
                                                    <div class="controls col-md-6">
                                                        <label>Max. Price </label>
                                                        <select id="max_price" name="max_price" class="form-control">
                                                            <option value=""> Max. price</option>
                                                            <option value="100">$100</option>
                                                            <option value="200">$200</option>
                                                            <option value="300">$300</option>
                                                            <option value="400">$400</option>
                                                            <option value="500">$500</option>
                                                            <option value="600">$600</option>
                                                            <option value="700">$700</option>
                                                            <option value="800">$800</option>
                                                            <option value="900">$900</option>
                                                            <option value="1000">$1000</option>
                                                            <option value="1500">$1500</option>
                                                            <option value="2000">$2000</option>
                                                            <option value="2500">$2500</option>
                                                            <option value="3000">$3000</option>
                                                        </select>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="control-group form-group">
                                                    <div class="controls col-md-6 first">
                                                        <label>Garages</label>
                                                        <input id="garages" class="form-control" type="text" name="garage" placeholder="Enter quantity">
                                                    </div>
                                                </div>
                                                <div class="control-group form-group">
                                                    <div class="controls col-md-6 first">
                                                        <label>Bathrooms</label>
                                                        <input id="bathrooms" class="form-control" type="text" name="bathroom" placeholder="Enter quantity">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <!-- For success/fail messages -->
                                                <button name="search_button" type="submit" class="btn btn-primary">Search</button>
                                                <button name="reset_button" type="submit" class="btn btn-primary float-right">Reset</button>
                                            </form>
                                        </div>
                                    </div>
                                    <?php
                                    echo "<div class='col-md-4'>";
                                }
                                else if($counter != 0)
                                {
                                    echo "<div class='col-md-4'>";
                                }
                                echo "<div class='panel panel-default'>";
                                echo "<div class='panel-image'>";
                                echo "<img class='img-responsive img-hover' src='".$row['photo_1']."'>";
                                echo "<div class='img_hov_eff'>";
                                echo "<a class='btn btn-default btn_trans' href='/property/id/".$row['property_id']."'> More Details </a>";
                                echo "</div>";
                                echo "</div>";
                                echo "<div class='sal_labl'>For ".$row['type']."</div>";
                                echo "<div class='panel-body'>";
                                echo "<div class='prop_feat'>";
                                echo "<p class='area'><i class='fa fa-home'></i> ".$row['area']." Sq Ft</p>";
                                echo "<p class='bedrom'><i class='fa fa-bed'></i> ".$row['bedroom']." Bedrooms</p>";
                                echo "<p class='bedrom'><i class='fa fa-car'></i> ".$row['garage']." Garage</p>";
                                echo "</div>";
                                echo "<h3 class='sec_titl'>".$row['name']."</h3>";
                                echo "<div class='description_div'>";
                                echo "<p class='sec_desc'>".$description."</p>";
                                echo "</div>";
                                echo "<div class='panel_bottom'>";
                                echo "<div class='col-md-12'>";
                                echo "<p class='price text-left'> $".$row['price']."</p>";
                                echo "</div>";
                                echo "</div>";
                                echo "</div>";
                                echo "</div>";
                                echo "</div>";
                                $property_left--;
                                $counter++;
                                if($counter == 3)
                                {
                                    echo "</div>";
                                    echo "<div class='spacer-30'></div>";
                                    $counter = 0;
                                }
                                else if($counter == 2 && $first_row == 1)
                                {
                                    echo "</div>";
                                    echo "<div class='spacer-30'></div>";
                                    $counter = 0;
                                    $first_row = 0;
                                }
                                else if($counter != 3 && $property_left == 0)
                                {
                                    echo "</div>";
                                    echo "<div class='spacer-30'></div>";
                                }
                            }
                            echo 'Страницы: ';
                            // _________________ начало блока 1 _________________
                            // Выводим ссылки "назад" и "на первую страницу"
                            if ($page_number>=1)
                            {
                                // Значение page_number= для первой страницы всегда равно единице,
                                // поэтому так и пишем
                                echo '<a href="1"><<</a> &nbsp; ';
                                // Так как мы количество страниц до этого уменьшили на единицу,
                                // то для того, чтобы попасть на предыдущую страницу,
                                // нам не нужно ничего вычислять
                                echo '<a href="'.$page_number.'">< </a> &nbsp; ';
                            }
                            // __________________ конец блока 1 __________________
                            // На данном этапе номер текущей страницы = $page_number+1
                            $current_page = $page_number + 1;
                            // Узнаем с какой ссылки начинать вывод
                            $start = $current_page - $limit;
                            // Узнаем номер последней ссылки для вывода
                            $end = $current_page + $limit;
                            // Выводим ссылки на все страницы
                            // Начальное число $j в нашем случае должно равнятся единице, а не нулю
                            for ($j = 1; $j < $recuired_pages; $j++)
                            {
                                // Выводим ссылки только в том случае, если их номер больше или равен
                                // начальному значению, и меньше или равен конечному значению
                                if ($j>=$start && $j<=$end)
                                {
                                    // Ссылка на текущую страницу выделяется жирным
                                    if ($j == ($page_number + 1))
                                    {
                                        echo '<a href="'.$j.'"><strong style="color: #df0000">'.$j.'</strong></a> &nbsp; ';
                                    }
                                    // Ссылки на остальные страницы
                                    else
                                    {
                                        echo '<a href="'.$j.'">'.$j.'</a> &nbsp; ';
                                    }
                                }
                            }
                            // _________________ начало блока 2 _________________
                            // Выводим ссылки "вперед" и "на последнюю страницу"
                            if ($j > $page_number && ($page_number + 2) < $j)
                            {
                                // Чтобы попасть на следующую страницу нужно увеличить $recuired_pages на 2
                                echo '<a href="'.($page_number + 2).'"> ></a> &nbsp; ';
                                // Так как у нас $j = количество страниц + 1, то теперь
                                // уменьшаем его на единицу и получаем ссылку на последнюю страницу
                                echo '<a href="'.($j - 1).'">>></a> &nbsp; ';
                            }
                            // __________________ конец блока 2 __________________
                        }
                        else {
                            ?>
                            <div class="row">
                                <!-- Search Form -->
                                <div class="col-md-4">
                                    <div class="srch_frm">
                                        <h3>Real Estate Search</h3>
                                        <?php echo form_open('property/all/1') ?>
                                            <div class="control-group form-group">
                                                <div class="controls">
                                                    <label>Location </label>
                                                    <select id="city" name="city" class="form-control">
                                                        <option value="">Choose location</option>
                                                        <option value="Alabama">Alabama</option>
                                                        <option value="Alaska">Alaska</option>
                                                        <option value="Arizona">Arizona</option>
                                                        <option value="Arkansas">Arkansas</option>
                                                        <option value="California">California</option>
                                                        <option value="Colorado">Colorado</option>
                                                        <option value="Connecticut">Connecticut</option>
                                                        <option value="Delaware">Delaware</option>
                                                        <option value="District Of Columbia">District Of Columbia</option>
                                                        <option value="Florida">Florida</option>
                                                        <option value="Georgia">Georgia</option>
                                                        <option value="Hawaii">Hawaii</option>
                                                        <option value="Idaho">Idaho</option>
                                                        <option value="Illinois">Illinois</option>
                                                        <option value="Indiana">Indiana</option>
                                                        <option value="Iowa">Iowa</option>
                                                        <option value="Kansas">Kansas</option>
                                                        <option value="Kentucky">Kentucky</option>
                                                        <option value="Louisiana">Louisiana</option>
                                                        <option value="Maine">Maine</option>
                                                        <option value="Maryland">Maryland</option>
                                                        <option value="Massachusetts">Massachusetts</option>
                                                        <option value="Michigan">Michigan</option>
                                                        <option value="Minnesota">Minnesota</option>
                                                        <option value="Mississippi">Mississippi</option>
                                                        <option value="Missouri">Missouri</option>
                                                        <option value="Montana">Montana</option>
                                                        <option value="Nebraska">Nebraska</option>
                                                        <option value="Nevada">Nevada</option>
                                                        <option value="New Hampshire">New Hampshire</option>
                                                        <option value="New Jersey">New Jersey</option>
                                                        <option value="New Mexico">New Mexico</option>
                                                        <option value="New York">New York</option>
                                                        <option value="North Carolina">North Carolina</option>
                                                        <option value="North Dakota">North Dakota</option>
                                                        <option value="Ohio">Ohio</option>
                                                        <option value="Oklahoma">Oklahoma</option>
                                                        <option value="Oregon">Oregon</option>
                                                        <option value="Pennsylvania">Pennsylvania</option>
                                                        <option value="Rhode Island">Rhode Island</option>
                                                        <option value="South Carolina">South Carolina</option>
                                                        <option value="South Dakota">South Dakota</option>
                                                        <option value="Tennessee">Tennessee</option>
                                                        <option value="Texas">Texas</option>
                                                        <option value="Utah">Utah</option>
                                                        <option value="Vermont">Vermont</option>
                                                        <option value="Virginia">Virginia</option>
                                                        <option value="Washington">Washington</option>
                                                        <option value="West Virginia">West Virginia</option>
                                                        <option value="Wisconsin">Wisconsin</option>
                                                        <option value="Wyoming">Wyoming</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="control-group form-group">
                                                <div class="controls col-md-6 first">
                                                    <label>Bedrooms</label>
                                                    <input id="bedrooms" class="form-control" type="text" name="bedroom" placeholder="Enter quantity">
                                                </div>
                                                <div class="controls col-md-6">
                                                    <label>Type </label>
                                                    <select id="type" name="type" class="form-control">
                                                        <option value="">Choose type</option>
                                                        <option value="Rent">For Rent</option>
                                                        <option value="Sale">For Sale</option>
                                                    </select>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="control-group form-group">
                                                <div class="controls col-md-6 first">
                                                    <label>Min. Price </label>
                                                    <select id="min_price" name="min_price" class="form-control">
                                                        <option value="">Min. price</option>
                                                        <option value="50">$50</option>
                                                        <option value="100">$100</option>
                                                        <option value="200">$200</option>
                                                        <option value="300">$300</option>
                                                        <option value="400">$400</option>
                                                        <option value="500">$500</option>
                                                        <option value="600">$600</option>
                                                        <option value="700">$700</option>
                                                        <option value="800">$800</option>
                                                        <option value="900">$900</option>
                                                        <option value="1000">$1000</option>
                                                        <option value="1500">$1500</option>
                                                        <option value="2000">$2000</option>
                                                        <option value="2500">$2500</option>
                                                    </select>
                                                </div>
                                                <div class="controls col-md-6">
                                                    <label>Max. Price </label>
                                                    <select id="max_price" name="max_price" class="form-control">
                                                        <option value=""> Max. price</option>
                                                        <option value="100">$100</option>
                                                        <option value="200">$200</option>
                                                        <option value="300">$300</option>
                                                        <option value="400">$400</option>
                                                        <option value="500">$500</option>
                                                        <option value="600">$600</option>
                                                        <option value="700">$700</option>
                                                        <option value="800">$800</option>
                                                        <option value="900">$900</option>
                                                        <option value="1000">$1000</option>
                                                        <option value="1500">$1500</option>
                                                        <option value="2000">$2000</option>
                                                        <option value="2500">$2500</option>
                                                        <option value="3000">$3000</option>
                                                    </select>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="control-group form-group">
                                                <div class="controls col-md-6 first">
                                                    <label>Garages</label>
                                                    <input id="garages" class="form-control" type="text" name="garage" placeholder="Enter quantity">
                                                </div>
                                            </div>
                                            <div class="control-group form-group">
                                                <div class="controls col-md-6 first">
                                                    <label>Bathrooms</label>
                                                    <input id="bathrooms" class="form-control" type="text" name="bathroom" placeholder="Enter quantity">
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <!-- For success/fail messages -->
                                            <button name="search_button" type="submit" class="btn btn-primary">Search</button>
                                            <button name="reset_button" type="submit" class="btn btn-primary float-right">Reset</button>
                                        </form>
                                    </div>
                                </div>

                            </div>
                            <?php

                        }
                     ?>
                </div>
                <!-- /.container -->
            </section>
            <div class="spacer-60"></div>
        </div>
    </div>
