<?php
class Admin extends CI_Controller{
    public function index()
    {
        if (!isset($_SESSION['email_agent']))
        {
            $this->load->view('admin/log_in');
        }
        else
        {
            redirect('admin/add_agent');
        }
    }
    public function add_agent()
    {
        if (isset($_SESSION['email_agent']))
        {
            $data['error'] = NULL;
            $data['title'] = 'Add agent';
            $this->load->view('admin/admin_templates/admin_header', $data);
            $this->load->view('admin/admin_templates/admin_navigation');
            $this->load->view('admin/add_agent');
            $this->load->view('admin/admin_templates/admin_footer');
        }
        else
        {
            redirect('admin');
        }
    }
    public function add_property()
    {
        if (isset($_SESSION['email_agent']))
        {
            $data['title'] = 'Add property';
            $this->load->view('admin/admin_templates/admin_header', $data);
            $this->load->view('admin/admin_templates/admin_navigation');
            $this->load->view('admin/add_property');
            $this->load->view('admin/admin_templates/admin_footer');
        }
        else
        {
            redirect('admin');
        }
    }
    public function log_out()
    {
        $this->session->sess_destroy();
        redirect('/admin');
    }
    public function add_agent_request()
    {
        $data = array('success' => FALSE, 'photo_success' => FALSE, 'messages' => array());
        $this->form_validation->set_rules('phone_number', 'Phone number', 'required|phone_reg_exp|is_phone_number_exist');
        $this->form_validation->set_rules('email', 'Email', 'required|email_reg_exp|is_email_exist_admin');
        $this->form_validation->set_rules('second_name', 'Second name', 'required');
        $this->form_validation->set_rules('first_name', 'First name', 'required');
        $this->form_validation->set_rules('skype', 'Skype', 'required');
        $this->form_validation->set_rules('about', 'About', 'required');
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        if ($this->form_validation->run() == TRUE)
        {
            $data['success'] = TRUE;
            if(isset($_FILES['photo']))
            {
                $config['upload_path']   = './uploaded_images/agent/';
                $config['allowed_types'] = 'jpg|png';
                $config['max_size']      = 2048;
                $config['max_width']     = 5000;
                $config['max_height']    = 5000;
                $this->upload->initialize($config);
                if($this->upload->do_upload('photo'))
                {
                    $data['photo_success'] = TRUE;
                    $photo = base_url().'/uploaded_images/agent/'.$this->upload->data('file_name');
                    $phone_number = $_POST['phone_number'];
                    $first_name = $_POST['first_name'];
                    $second_name = $_POST['second_name'];
                    $email = $_POST['email'];
                    $skype = $_POST['skype'];
                    $about = $_POST['about'];
                    $chars = "qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";
                    $max = 10;
                    $size = StrLen($chars) - 1;
                    $password = null;
                    while($max--)
                    {
                        $password .= $chars[rand(0, $size)];
                    }
                    $insert_data = array(
                        'first_name' => $first_name,
                        'second_name' => $second_name,
                        'photo' => $photo,
                        'phone_number' => $phone_number,
                        'email' => $email,
                        'password' => md5($password),
                        'skype' => $skype,
                        'about' => $about
                    );
                    $this->new_data->add_agent($insert_data);
                    $this->email->from('wedoor.com', '');
                    $this->email->to($email);
                    $this->email->subject('Registration');
                    $this->email->message('You were successfully registered. Your email: '.$email.' Your password: '.$password);
                    $this->email->send();
                }
                else
                {
                    $data['messages']['photo'] = $this->upload->display_errors('<p class="text-danger">', '</p>');
                }
            }
            else
            {
                $data['messages']['photo'] = '<p class="text-danger">You need to choose file!</p>';
            }
        }
        else
        {
            if(!isset($_FILES['photo']))
            {
                $data['messages']['photo'] = '<p class="text-danger">You need to choose file!</p>';
            }
            else
            {
                $data['messages']['photo'] = '';
            }
            foreach ($_POST as $key => $value)
            {
                $data['messages'][$key] = form_error($key);
            }
        }
        echo json_encode($data);
    }
    public function log_in_request()
    {
        $data = array('success' => FALSE, 'messages' => array());
        $email = $_POST['email'];
        $this->form_validation->set_rules('email', 'email', 'required|email_reg_exp|check_email');
        $this->form_validation->set_rules('password', 'Password', "required|password_reg_exp|check_password_admin[$email]");
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        if ($this->form_validation->run() == TRUE)
        {
            $_SESSION['email_agent'] = $email;
            $data['success'] = TRUE;
        }
        else
        {
            foreach ($_POST as $key => $value)
            {
                $data['messages'][$key] = form_error($key);
            }
        }
        echo json_encode($data);
    }
    public function add_property_request()
    {
        $data = array('success' => FALSE, 'photo_success' => FALSE, 'messages' => array());
        $this->form_validation->set_rules('name', 'Name', 'required|is_name_exist');
        $this->form_validation->set_rules('price', 'Price', 'required|number_reg_exp');
        $this->form_validation->set_rules('type', 'Type', 'required',
            array('required' => 'You need to choose type!')
        );
        $this->form_validation->set_rules('city', 'City', 'required');
        $this->form_validation->set_rules('area', 'Area', 'required|number_reg_exp');
        $this->form_validation->set_rules('garage', 'Garages', 'required|number_reg_exp');
        $this->form_validation->set_rules('bedroom', 'Bedrooms', 'required|number_reg_exp');
        $this->form_validation->set_rules('bathroom', 'Bathrooms', 'required|number_reg_exp');
        $this->form_validation->set_rules('acres', 'Acres', 'required|number_reg_exp');
        $this->form_validation->set_rules('heat', 'Heat', 'required');
        $this->form_validation->set_rules('dimensions', 'dimensions', 'required|dimensions_reg_exp');
        $this->form_validation->set_rules('size_source', 'Size source', 'required');
        $this->form_validation->set_rules('ac', 'AC', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        if ($this->form_validation->run() == TRUE)
        {
            $data['success'] = TRUE;
            if (isset($_FILES['photos']))
            {
                $uploaded_photos_counter = 0;
                $photos_counter = count($_FILES['photos']['name']);
                for ($i = 0; $i <= $photos_counter - 1; $i++)
                {
                    $_FILES['photo_'.$i]['name']     = $_FILES['photos']['name'][$i];
                    $_FILES['photo_'.$i]['type']     = $_FILES['photos']['type'][$i];
                    $_FILES['photo_'.$i]['tmp_name'] = $_FILES['photos']['tmp_name'][$i];
                    $_FILES['photo_'.$i]['error']    = $_FILES['photos']['error'][$i];
                    $_FILES['photo_'.$i]['size']     = $_FILES['photos']['size'][$i];
                }
                $config['upload_path']   = './uploaded_images/property/';
                $config['allowed_types'] = 'jpg|png';
                $config['max_size']      = 4000;
                $config['max_width']     = 5000;
                $config['max_height']    = 5000;
                $this->upload->initialize($config);
                for ($i = 0; $i <= $photos_counter - 1; $i++)
                {
                    if($this->upload->do_upload('photo_'.$i))
                    {
                        $uploaded_photos_counter++;
                        $files_data['photo_'.$i]['path'] = $this->upload->data('full_path');
                        $files_data['photo_'.$i]['name'] = base_url().'./uploaded_images/property/'.$this->upload->data('file_name');
                    }
                }
                if($uploaded_photos_counter == $photos_counter)
                {
                    $data['photo_success'] = TRUE;
                    $name = $_POST['name'];
                    $price = $_POST['price'];
                    $type = $_POST['type'];
                    $city = $_POST['city'];
                    $area = $_POST['area'];
                    $garage = $_POST['garage'];
                    $bedroom = $_POST['bedroom'];
                    $bathroom = $_POST['bathroom'];
                    $acres = $_POST['acres'];
                    $heat = $_POST['heat'];
                    $dimensions = $_POST['dimensions'];
                    $size_source = $_POST['size_source'];
                    $ac = $_POST['ac'];
                    $description = $_POST['description'];
                    for ($i = 0; $i <= $uploaded_photos_counter - 1; $i++)
                    {
                        $photos[$i] = $files_data['photo_'.$i]['name'];
                    }
                    for ($i = $uploaded_photos_counter; $i <= 4; $i++) {
                        $photos[$i] = '';
                    }
                    $insert_data = array(
                        'name' => $name,
                        'price' => $price,
                        'type' => $type,
                        'city' => $city,
                        'area' => $area,
                        'garage' => $garage,
                        'bedroom' => $bedroom,
                        'bathroom' => $bathroom,
                        'acres' => $acres,
                        'heat' => $heat,
                        'dimensions' => $dimensions,
                        'size_source' => $size_source,
                        'ac' => $ac,
                        'photo_1' => $photos[0],
                        'photo_2' => $photos[1],
                        'photo_3' => $photos[2],
                        'photo_4' => $photos[3],
                        'photo_5' => $photos[4],
                        'description' => $description);
                    $this->new_data->add_property($insert_data);
                }
                else
                {
                    for($i = 0; $i <= $photo_counter - 1; $i++)
                    {
                        unlink($files_data['photo_'.$i]['path']);
                    }
                }
            }
            else
            {
                $data['messages']['photos'] = '<p class="text-danger">You need to choose file!</p>';
            }
        }
        else
        {
            if (!isset($_FILES['photos']))
            {
                $data['messages']['photos'] = '<p class="text-danger">You need to choose file!</p>';
            }
            else
            {
                $data['messages']['photos'] = '';
            }
            foreach ($_POST as $key => $value)
            {
                $data['messages'][$key] = form_error($key);
            }
        }
        echo json_encode($data);
    }
}
