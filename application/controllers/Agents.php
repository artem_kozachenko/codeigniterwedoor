<?php
class Agents extends CI_Controller{
    public function all($page_number)
    {
        $data['title'] = 'Our agents';
        $data['page_number'] = $page_number;
        $this->load->view('main/main_templates/main_header', $data);
        $this->load->view('main/our_agents', $data);
        $this->load->view('main/main_templates/main_footer');
    }
    public function id($agent_id)
    {
        $data['title'] = 'Agent';
        $data['agent_id'] = $agent_id;
        $this->load->view('main/main_templates/main_header', $data);
        $this->load->view('main/agent', $data);
        $this->load->view('main/main_templates/main_footer');
    }
    public function message_request()
    {
        $data = array('success' => FALSE, 'send' => FALSE, 'messages' => array());
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|email_reg_exp');
        $this->form_validation->set_rules('message', 'Message', 'required');
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        if($this->form_validation->run() == TRUE)
        {
            $data['success'] = TRUE;
            $query = $this->get_data->get_email_agent($_SESSION['agent_id']);
            foreach ($query->result_array() as $row)
            {
                $agent_email = $row['email'];
            }
            $this->email->from($_POST['email'], $_POST['name']);
            $this->email->to($agent_email);
            $this->email->message($_POST['message']);
            if ($this->email->send() == TRUE)
            {
                $data['send'] = TRUE;
            }
        }
        else
        {
            foreach ($_POST as $key => $value)
            {
                $data['messages'][$key] = form_error($key);
            }
        }
        echo json_encode($data);
    }
}
