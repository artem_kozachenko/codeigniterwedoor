<?php
class Property extends CI_Controller
{
    public function all($page_number)
    {
        if(!isset($_SESSION['post']) OR strpos(uri_string(), 'property/all') !== FALSE)
        {
            $_SESSION['post'] = NULL;
        }
        if(isset($_POST['search_button']))
        {
            $_SESSION['post'] = NULL;
            foreach ($_POST as $key => $value)
            {
                if($_POST[$key] != '' && $_POST[$key] != 'button')
                {
                    $_SESSION['post'][$key] = $value;
                }
            }
            if ($_SESSION['post'] == NULL) {
                $_SESSION['post'] = 1;
            }
            redirect('property/search/1');
        }
        else if(isset($_POST['reset_button']))
        {
            $_SESSION['post'] = NULL;
        }
        $data['title'] = 'All property';
        $data['page_number'] = $page_number;
        $this->load->view('main/main_templates/main_header', $data);
        $this->load->view('main/property_listings', $data);
        $this->load->view('main/main_templates/main_footer');
    }
    public function id($id)
    {
        if(isset($_POST['new_user_button']))
        {
            $this->register_login->add_new_user();
        }
        if(isset($_POST['log_in_button']))
        {
            $this->register_login->log_in();
        }
        if(isset($_POST['log_out_button']))
        {
            $this->register_login->log_out();
        }
        $data['title'] = 'Property';
        $data['property_id'] = $id;
        $this->load->view('main/main_templates/main_header', $data);
        $this->load->view('main/property', $data);
        $this->load->view('main/main_templates/main_footer');
    }
}
