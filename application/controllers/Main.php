<?php
class Main extends CI_Controller{
    public function index()
    {
        $data['title'] = 'Home';
        $this->load->view('main/main_templates/main_header', $data);
        $this->load->view('main/home');
        $this->load->view('main/main_templates/main_footer');
    }
    public function contact()
    {
        $data['title'] = 'Contact';
        $this->load->view('main/main_templates/main_header', $data);
        $this->load->view('main/contact');
        $this->load->view('main/main_templates/main_footer');
    }
    public function send_message_request()
    {
        $data = array('success' => FALSE, 'messages' => array(), 'delivered' => FALSE);
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|email_reg_exp');
        $this->form_validation->set_rules('phone_number', 'Phone number', 'required|phone_reg_exp');
        $this->form_validation->set_rules('website', 'Website', 'required');
        $this->form_validation->set_rules('message', 'Message', 'required');
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        if($this->form_validation->run() == TRUE)
        {
            $data['success'] = TRUE;
            $email = $_POST['email'];
            $name = $_POST['name'];
            $phone_number = $_POST['phone_number'];
            $website = $_POST['website'];
            $message = $_POST['message'];
            $this->email->from($email, $name);
            $this->email->to('weedor@realestate.com');
            $this->email->message($message.' My phone: '.$phone_number.". My website: ".$website);
            if ($this->email->send() == TRUE)
            {
                $data['delivered'] = TRUE;
                $data['messages']['success'] = 'Your message was successfully delivered!';
            }
            else
            {
                $data['messages']['unsuccess'] = 'Your message wasn\'t delivered!';
            }
        }
        else
        {
            foreach ($_POST as $key => $value)
            {
                $data['messages'][$key] = form_error($key);
            }
        }
        echo json_encode($data);
    }
    public function registration_request()
    {
        $data = array('success' => FALSE, 'messages' => array(), 'delivered' => FALSE);
        $password = $_POST['password-reg'];
        $this->form_validation->set_rules('username-reg', 'Username', 'required|is_username_exist');
        $this->form_validation->set_rules('email-reg', 'Email', 'required|email_reg_exp|is_email_exist');
        $this->form_validation->set_rules('password-reg', 'Password', 'required|password_reg_exp');
        $this->form_validation->set_rules('confirm_password', 'Confirm password', "required|password_reg_exp|same_password[$password]");
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        if ($this->form_validation->run() == TRUE )
        {
            $data['success'] = TRUE;
            $username = $_POST['username-reg'];
            $email = $_POST['email-reg'];
            $insert_data = array(
                'username' => $username,
                'email' => $email,
                'password' => md5($password)
            );
            $this->new_data->add_user($insert_data);
            $_SESSION['username'] = $username;
            $_SESSION['email'] = $email;
        }
        else
        {
            foreach ($_POST as $key => $value)
            {
                $data['messages'][$key] = form_error($key);
            }
        }
        echo json_encode($data);
    }
    public function log_in_request()
    {
        $data = array('success' => FALSE, 'messages' => array(), 'delivered' => FALSE);
        $username = $_POST['username_log_in'];
        $this->form_validation->set_rules('username_log_in', 'Username', 'required|check_username');
        $this->form_validation->set_rules('password_log_in', 'Password', "required|password_reg_exp|check_password[$username]");
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        if ($this->form_validation->run() == TRUE )
        {
            $data['success'] = TRUE;
            $query = $this->get_data->get_email($_POST['username_log_in']);
            foreach ($query->result_array() as $row)
            {
                $email = $row['email'];
            }
            $_SESSION['username'] = $username;
            $_SESSION['email'] = $email;
        }
        else
        {
            foreach ($_POST as $key => $value)
            {
                $data['messages'][$key] = form_error($key);
            }
        }
        echo json_encode($data);
    }
    public function log_out_request()
    {
        $this->session->sess_destroy();
    }
}
