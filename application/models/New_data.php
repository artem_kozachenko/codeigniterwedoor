<?php
class New_data extends CI_Model
{
    public function add_user($insert_data)
    {
        $this->db->insert('users', $insert_data);
    }
    public function add_agent($insert_data)
    {
        $this->db->insert('agents', $insert_data);
    }
    public function add_property($insert_data)
    {
        $this->db->insert('property', $insert_data);
    }
}
