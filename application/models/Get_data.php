<?php
class Get_data extends CI_Model
{
    public function get_email($username)
    {
        $this->db->select('email');
        $query = $this->db->get_where('users', array('username' => $username), 1);
        return $query;
    }
    public function all_agents($agent_limit, $offset)
    {
        $this->db->select('agent_id, first_name, second_name, photo, phone_number, email, skype, about');
        $query = $this->db->get('agents', $agent_limit, $offset);
        return $query;
    }
    public function all_property($property_limit, $offset)
    {
        $this->db->select('property_id, name, price, type, area, garage, bedroom, photo_1, description');
        $query = $this->db->get('property', $property_limit, $offset);
        return $query;
    }
    public function search_property($property_limit, $offset, $post)
    {
        $this->db->select('property_id, name, price, type, area, garage, bedroom, photo_1, description');
        if (!isset($post['min_price']) && !isset($post['max_price']))
        {
            $this->db->where($post);
        }
        if(isset($post['min_price']) && !isset($post['max_price']))
        {
            $this->db->where('price >=', $post['min_price']);
        }
        else if(isset($post['max_price']) && !isset($post['min_price']))
        {
            $this->db->where('price <=', $post['max_price']);
        }
        else if(isset($post['min_price']) && isset($post['max_price']))
        {
            $where = 'price BETWEEN '.$post['min_price'].' AND '.$post['max_price'].'';
            $this->db->where($where);
        }
        foreach ($post as $key => $value)
        {
            if($key != 'min_price' && $key != 'max_price')
            {
                $this->db->where($key, $value);
            }
        }
        $query = $this->db->get('property', $property_limit, $offset);
        return $query;
    }
    public function one_agent($agent_id)
    {
        $query = $this->db->get_where('agents', array('agent_id' => $agent_id) , 1);
        return $query;
    }
    public function one_property($property_id)
    {
        $query = $this->db->get_where('property', array('property_id' => $property_id) , 1);
        return $query;
    }
    public function get_email_agent($agent_id)
    {
        $this->db->select('email');
        $query = $this->db->get_where('agents', array('agent_id' => $agent_id) , 1);
        return $query;
    }
    public function random_agent($random_position)
    {
        $this->db->select('first_name, second_name, photo, phone_number, email, skype, about');
        $query = $this->db->get('agents', 1, $random_position);
        return $query;
    }
    public function get_3_property($random_position)
    {
        $this->db->select('property_id, name, price, type, area, garage, bedroom, photo_1, description');
        $query = $this->db->get('property', 3, $random_position);
        return $query;
    }
    public function get_4_agents($random_position)
    {
        $this->db->select('agent_id, first_name, second_name, phone_number, photo');
        $query = $this->db->get('agents', 4, $random_position);
        return $query;
    }
    public function get_5_property($random_position)
    {
        $this->db->select('property_id, name, price, photo_1, description');
        $query = $this->db->get('property', 5, $random_position);
        return $query;
    }
}
