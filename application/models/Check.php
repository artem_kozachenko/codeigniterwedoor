<?php
class Check extends CI_Model
{
    public function find_email_users($email)
    {
        $this->db->select('email');
        $query = $this->db->get_where('users', array('email' => $email), 1);
        return $query;
    }
    public function find_email_agents($email)
    {
        $this->db->select('email');
        $query = $this->db->get_where('agents', array('email' => $email), 1);
        return $query;
    }
    public function find_username_users($username)
    {
        $this->db->select('username');
        $query = $this->db->get_where('users', array('username' => $username), 1);
        return $query;
    }
    public function find_username_agents($username)
    {
        $this->db->select('username');
        $query = $this->db->get_where('agents', array('username' => $username), 1);
        return $query;
    }
    public function find_phone_number($phone_number)
    {
        $this->db->select('phone_number');
        $query = $this->db->get_where('agents', array('phone_number' => $phone_number), 1);
        return $query;
    }
    public function find_name_property($name)
    {
        $this->db->select('name');
        $query = $this->db->get_where('property', array('name' => $name), 1);
        return $query;
    }
    public function is_username_right($username)
    {
        $this->db->select('username');
        $query = $this->db->get_where('users', array('username' => $username), 1);
        return $query;
    }
    public function is_password_right($password, $username)
    {
        $this->db->select('password');
        $query = $this->db->get_where('users', array('username' => $username, 'password' => $password), 1);
        return $query;
    }
    public function is_email_right($email)
    {
        $this->db->select('email');
        $query = $this->db->get_where('agents', array('email' => $email), 1);
        return $query;
    }
    public function is_password_right_agents($password, $email)
    {
        $this->db->select('password');
        $query = $this->db->get_where('agents', array('email' => $email, 'password' => $password), 1);
        return $query;
    }

}
