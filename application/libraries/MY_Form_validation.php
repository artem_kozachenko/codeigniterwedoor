<?php
class MY_Form_validation extends CI_Form_validation{
    protected $CI;
    public function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
    }
    public function same_password($retype_password, $password)
    {
        if ($password == $retype_password) {
            return TRUE;
        }
        else {
            $this->CI->form_validation->set_message('same_password', 'Passwords do not match!');
            return FALSE;
        }
    }
    public function is_username_exist($username)
    {
        $username_result = '';
        $query = $this->CI->check->find_username_users($username);
        foreach ($query->result_array() as $row)
        {
            $username_result = $row['username'];
        }
        if ($username_result == $username)
        {
            $this->CI->form_validation->set_message('is_username_exist', 'Such username already exist!');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    public function is_email_exist($email)
    {
        $email_result = '';
        $query = $this->CI->check->find_email_users($email);
        foreach ($query->result_array() as $row)
        {
            $email_result = $row['email'];
        }
        if ($email_result == $email)
        {
            $this->CI->form_validation->set_message('is_email_exist', 'Such email already exist!');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    public function check_username($username)
    {
        $username_result = '';
        $query = $this->CI->check->is_username_right($username);
        foreach ($query->result_array() as $row)
        {
            $username_result = $row['username'];
        }
        if ($username_result == $username)
        {
            return TRUE;
        }
        else
        {
            $this->CI->form_validation->set_message('check_username', 'Invalid username');
            return FALSE;
        }
    }
    public function check_password($password, $username)
    {
        $password = md5($password);
        $password_result = '';
        $query = $this->CI->check->is_password_right($password, $username);
        foreach ($query->result_array() as $row)
        {
            $password_result = $row['password'];
        }
        if ($password_result == $password)
        {
            return TRUE;
        }
        else
        {
            $this->CI->form_validation->set_message('check_password', 'Your username/password combination is invalid');
            return FALSE;
        }
    }
    public function check_email($email)
    {
        $email_result = '';
        $query = $this->CI->check->is_email_right($email);
        foreach ($query->result_array() as $row)
        {
            $email_result = $row['email'];
        }
        if ($email_result == $email)
        {
            return TRUE;
        }
        else
        {
            $this->CI->form_validation->set_message('check_email', 'Invalid email');
            return FALSE;
        }
    }
    public function check_password_admin($password, $email)
    {
        $password = md5($password);
        $password_result = '';
        $query = $this->CI->check->is_password_right_agents($password, $email);
        foreach ($query->result_array() as $row)
        {
            $password_result = $row['password'];
        }
        if ($password_result == $password)
        {
            return TRUE;
        }
        else
        {
            $this->CI->form_validation->set_message('check_password_admin', 'Your email/password combination is invalid');
            return FALSE;
        }
    }
    public function is_phone_number_exist($phone_number)
    {
        $phone_number_result = '';
        $query = $this->CI->check->find_phone_number($phone_number);
        foreach ($query->result_array() as $row)
        {
            $phone_number_result = $row['phone_number'];
        }
        if ($phone_number_result == $phone_number && $phone_number != '')
        {
            $this->CI->form_validation->set_message('is_phone_number_exist', 'Such phone number already exist!');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    public function is_email_exist_admin($email)
    {
        $email_result = '';
        $query = $this->CI->check->find_email_agents($email);
        foreach ($query->result_array() as $row)
        {
            $email_result = $row['email'];
        }
        if ($email_result == $email && $email != '')
        {
            $this->CI->form_validation->set_message('is_email_exist_admin', 'Such email already exist!');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    public function is_name_exist($name)
    {
        $name_result = '';
        $query = $this->CI->check->find_name_property($name);
        foreach ($query->result_array() as $row)
        {
            $name_result = $row['name'];
        }
        if ($name_result == $name && $name != '')
        {
            $this->CI->form_validation->set_message('is_name_exist', 'Such name already exist!');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    public function email_reg_exp($email)
    {
        $pattern = '/^[0-9A-Za-z]{1}[-0-9A-z\.]{0,}(?<![.-_])@([-A-Za-z]{1,}\.){1,2}[-A-Za-z]{2,}$/';
        if (preg_match($pattern, $email) == TRUE) {
            return TRUE;
        }
        else
        {
            $this->CI->form_validation->set_message('email_reg_exp', 'Wrong email format!');
            return FALSE;
        }
    }
    public function phone_reg_exp($phone_number)
    {
        $pattern = '/[0-9]{1,15}/';
        if (preg_match($pattern, $phone_number) == TRUE) {
            return TRUE;
        }
        else
        {
            $this->CI->form_validation->set_message('phone_reg_exp', 'Phone number can only contain numbers!');
            return FALSE;
        }
    }
    public function password_reg_exp($password)
    {
        $pattern = '/[0-9a-zA-Z]{1,32}/';
        if (preg_match($pattern, $password) == TRUE) {
            return TRUE;
        }
        else
        {
            $this->CI->form_validation->set_message('password_reg_exp', 'Password can only contain Latin characters and numbers!');
            return FALSE;
        }
    }
    public function number_reg_exp($number)
    {
        $pattern = '/[0-9]{1,}/';
        if (preg_match($pattern, $number) == TRUE) {
            return TRUE;
        }
        else
        {
            $this->CI->form_validation->set_message('number_reg_exp', 'The value must be a number!');
            return FALSE;
        }
    }
    public function dimensions_reg_exp($dimensions)
    {
        $pattern = '/[0-9]{1,}x[0-9]{1,}/';
        if (preg_match($pattern, $dimensions) == TRUE) {
            return TRUE;
        }
        else
        {
            $this->CI->form_validation->set_message('dimensions_reg_exp', 'Wrong dimensions format!');
            return FALSE;
        }
    }









}
